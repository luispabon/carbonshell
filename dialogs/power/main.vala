enum Resp {
    REBOOT, SHUTDOWN, FW, HIBERNATE
}

[DBus (name = "org.freedesktop.login1.Manager")]
interface LoginInterface : Object {
    public abstract void power_off(bool interactive) throws Error;
    public abstract void hibernate(bool interactive) throws Error;
	public abstract string can_hibernate() throws Error;

	public abstract void reboot(bool interactive) throws Error;
	public abstract string can_reboot_to_firmware_setup() throws Error;
	public abstract void set_reboot_to_firmware_setup(bool val) throws Error;
}

public int main(string[] args) {
	Gtk.init(ref args);
	var timeout_msg = "The system will automatically power off in <tt>%s</tt>";

    LoginInterface mgr;
    try {
        mgr = Bus.get_proxy_sync(BusType.SYSTEM, "org.freedesktop.login1",
            "/org/freedesktop/login1");
    } catch (Error e) {
        error(e.message);
        return 1;
    }
    var can_hibernate = mgr.can_hibernate() == "yes";
    var can_firmware = mgr.can_reboot_to_firmware_setup() == "yes";

	var pwr_dialog = new Gtk.MessageDialog(
	    null,
	    Gtk.DialogFlags.MODAL,
	    Gtk.MessageType.OTHER,
	    Gtk.ButtonsType.CANCEL,
	    "Power Off"
	);
    pwr_dialog.secondary_text = timeout_msg.printf("1:00");
    pwr_dialog.secondary_use_markup = true;
	var elapsed = 1;
    var timed_out = false;
	Timeout.add(1000, () => {
	    pwr_dialog.secondary_text = timeout_msg.printf("0:%.2d".printf(60 - elapsed));
	    elapsed++;
	    if (elapsed > 60) {
	        timed_out = true;
	        pwr_dialog.response(Resp.SHUTDOWN);
	        return Source.REMOVE;
	    } else return Source.CONTINUE;
	});
	var restart = pwr_dialog.add_button("_Restart", Resp.REBOOT) as Gtk.Button;
	var pwroff = pwr_dialog.add_button("_Shutdown", Resp.SHUTDOWN) as Gtk.Button;
	var content = pwr_dialog.message_area as Gtk.Box;
	pwr_dialog.destroy.connect(Gtk.main_quit);

    bool ctrl_held = false;
    pwr_dialog.key_press_event.connect(evt => {
        if (evt.keyval == Gdk.Key.Control_L) {
            ctrl_held = true;
            if (can_firmware)
                restart.label = "_Firmware";
            if (can_hibernate)
                pwroff.label = "_Hibernate";
        }
        return Gdk.EVENT_PROPAGATE;
    });
    pwr_dialog.key_release_event.connect(evt => {
        if (evt.keyval == Gdk.Key.Control_L) {
            ctrl_held = false;
            restart.label = "_Restart";
            pwroff.label = "_Shutdown";
        }
        return Gdk.EVENT_PROPAGATE;
    });

    //TODO: List apps that are inhibiting shutdown

	pwr_dialog.response.connect(result => {
	    if (result == Gtk.ResponseType.CANCEL) return;
	    if (ctrl_held && !timed_out) result += 2;

		switch (result) {
		    case Resp.HIBERNATE:
		        if (can_hibernate)
		            mgr.hibernate(true);
		        else
		            mgr.power_off(true);
		        break;
	        case Resp.SHUTDOWN:
	            mgr.power_off(true);
	            break;
		    case Resp.FW:
		        if (can_firmware)
		            mgr.set_reboot_to_firmware_setup(true);
		        mgr.reboot(true);
		        break; // For some reason vala won't compile with fall thru
	        case Resp.REBOOT:
		        mgr.reboot(true);
		        break;
		}
	});

    content.show_all();

    LayerShell.init_for_window(pwr_dialog);
    LayerShell.set_layer(pwr_dialog, LayerShell.Layer.TOP);
    LayerShell.set_keyboard_interactivity(pwr_dialog, true);
    pwr_dialog.decorated = true;
	pwr_dialog.run();
	return 0;
}
