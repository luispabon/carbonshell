[DBus (name = "org.freedesktop.login1.Manager")]
interface LoginInterface : Object {
	public abstract UnixInputStream inhibit(string what, string who,
	    string why, string mode) throws Error;
}

void main() {
    try {
        print("[pwr-btn-inhibit] Starting...\n");
        LoginInterface mgr = Bus.get_proxy_sync(BusType.SYSTEM,
            "org.freedesktop.login1", "/org/freedesktop/login1");
        var fd = mgr.inhibit("handle-power-key", "carbonSHELL",
            "Graphical power menu", "block");

        print("[pwr-btn-inhibit] Inhibited. Going to sleep\n");
        Posix.pause();
    } catch (Error e) {
        error(e.message);
    }
}
