public int main(string[] args) {
	Gtk.init(ref args);
	Settings settings = new Settings("sh.carbon.run");

	var run_dialog = new Gtk.MessageDialog.with_markup(
	    null,
	    Gtk.DialogFlags.MODAL,
	    Gtk.MessageType.OTHER,
	    Gtk.ButtonsType.CANCEL,
	    "<big><b>Enter a command</b></big>"
	);
	run_dialog.add_button("_Run", Gtk.ResponseType.ACCEPT);
	var content = run_dialog.message_area as Gtk.Box;
	run_dialog.destroy.connect(Gtk.main_quit);

	var run_entry = new Gtk.Entry();
	run_entry.width_chars = 30;
	run_entry.activate.connect(() => run_dialog.response(Gtk.ResponseType.ACCEPT));
	content.pack_start(run_entry);

	run_entry.completion = new Gtk.EntryCompletion();
	run_entry.completion.inline_completion = true;
	run_entry.completion.inline_selection = true;
	run_entry.completion.popup_single_match = false;
	run_entry.completion.minimum_key_length = 0;
    Gtk.ListStore command_history = new Gtk.ListStore(1, typeof(string));
	run_entry.completion.model = command_history;
	run_entry.completion.text_column = 0;
	Gtk.TreeIter iter;
	foreach (string command in settings.get_strv("history")) {
		command_history.append(out iter);
		command_history.set(iter, 0, command);
	}

	var in_term = new Gtk.CheckButton.with_mnemonic("Open in _terminal");
	in_term.active = settings.get_boolean("run-in-term");
	content.pack_start(in_term);

	run_dialog.response.connect(result => {
	    if (result != Gtk.ResponseType.ACCEPT) return;
		string command = run_entry.text;

		string[] history = settings.get_strv("history");
		if (!(command in history) && command != "") {
			history += command;
			settings.set_strv("history", history);
		}
		settings.set_boolean("run-in-term", in_term.active);

		string cmdline = "bash -c %s".printf(Shell.quote(command));
		info("Running command: %s", cmdline);
        var ctx = Gdk.Display.get_default().get_app_launch_context();
        var flags = in_term.active ?
            AppInfoCreateFlags.NEEDS_TERMINAL : AppInfoCreateFlags.NONE;
        var info = AppInfo.create_from_commandline(cmdline, null, flags);
        info.launch(null, ctx);
	});

    content.show_all();

    LayerShell.init_for_window(run_dialog);
    LayerShell.set_layer(run_dialog, LayerShell.Layer.TOP);
    LayerShell.set_keyboard_interactivity(run_dialog, true);
    run_dialog.decorated = true;
	run_dialog.run();
	return 0;
}
