private void update_rotation_toggle(Panel.CCToggle toggle, Settings settings) {
    if (!toggle.active) {
        toggle.status = "Locked";
        toggle.icon_name = "rotation-locked-symbolic";
    } else {
        toggle.status = "Unlocked";
        toggle.icon_name = "rotation-allowed-symbolic";
    }
    settings.set_boolean("orientation-lock", toggle.active);
}

public Panel.Applet? create_applet() {
    var sig = Panel.Signals.obtain();
    var settings = new Settings("org.gnome.settings-daemon.peripherals.touchscreen");
    var toggle = new Panel.CCToggle();
    toggle.name = "Rotation";
    toggle.active = settings.get_boolean("orientation-lock");
    toggle.notify["active"].connect(() =>
        update_rotation_toggle(toggle, settings));
    update_rotation_toggle(toggle, settings);
    sig.register_cc_toggle(toggle);

    return null;
}
