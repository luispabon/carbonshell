class Notif.Applet : Panel.Applet {
    construct {
        applet_name = "notif";
        var server = Server.obtain();
        var notifications = new HashTable<uint32, Gtk.Revealer>(null, null);
        var dnd_settings = new Settings("sh.carbon.shell.panel");

        var btn = Panel.create_button();
        var btn_layout = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 4);
        var img = new Gtk.Image();
        img.pixel_size = 18;
        btn_layout.add(img);
        var notif_counter = new Gtk.Label("0");
        notif_counter.width_chars = 2;
        btn_layout.add(notif_counter);
        btn.add(btn_layout);
        this.panel_item = btn;

        // Set up Do Not Disturb settings bindings
        dnd_settings.bind("dnd", notif_counter, "visible",
            SettingsBindFlags.INVERT_BOOLEAN);
        notif_counter.bind_property("visible", btn, "tooltip-text",
            BindingFlags.SYNC_CREATE, (_, from, ref to) => {
                to = (from.get_boolean()) ? "Notifications" :
                    "Do Not Disturb Enabled";
                return true;
            });
        notif_counter.bind_property("visible", img, "icon-name",
            BindingFlags.SYNC_CREATE, (_, from, ref to) => {
                to = (from.get_boolean()) ?
                    "preferences-system-notifications-symbolic" :
                    "notifications-disabled-symbolic";
                return true;
            });

        var popover = Panel.create_popover(this, btn);
		popover.width_request = 375;
		
		var switcher = new Gtk.Stack();
        switcher.transition_type = Gtk.StackTransitionType.CROSSFADE;
        popover.add(switcher);

        var empty_state = new Dazzle.EmptyState();
        empty_state.pixel_size = 100;
        empty_state.icon_name = "emblem-ok-symbolic";
        empty_state.title = "No Notifications";
        empty_state.margin_top = 18;
        switcher.add_named(empty_state, "empty");

        var layout = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
        var clear_button = new Gtk.Button.with_label("Clear All");
        clear_button.clicked.connect(() => {
            foreach (var notif in server.get_all())
                notif.dismiss();
            switcher.visible_child_name = "empty";
        });
        layout.pack_end(clear_button, false);
        var viewport = new Gtk.ScrolledWindow(null, null);
        viewport.hscrollbar_policy = Gtk.PolicyType.NEVER;
        viewport.max_content_height = 500;
        viewport.propagate_natural_height = true;
        layout.pack_start(viewport);
        var notif_box = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
        viewport.add(notif_box);
        switcher.add_named(layout, "main");

        var notif_box_critical = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
        notif_box.add(notif_box_critical);
        var notif_box_normal = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
        notif_box.add(notif_box_normal);
        var notif_box_low = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
        notif_box.add(notif_box_low);

        switcher.show_all();
        popover.add(switcher);

        // Setup server plumbing
        server.notif_added.connect(notif => {
            var n_notifs = server.get_n_notifications();
            if (n_notifs > 99)
                notif_counter.label = "∞";
            else
                notif_counter.label = "%d".printf(n_notifs);

            Gtk.Revealer anim = new Gtk.Revealer();
            anim.transition_type = Gtk.RevealerTransitionType.SLIDE_DOWN;
            anim.transition_duration = 500;
            anim.add(create_notification(notif));
            notif.thaw_notify(); // Populate the widgets

            notif.notify["urgency"].connect(() => {
                var og_parent = anim.get_parent();
                if (og_parent != null)
                    og_parent.remove(anim);

                if (notif.urgency == 2)
                    notif_box_critical.add(anim);
                else if (notif.urgency == 1)
                    notif_box_normal.add(anim);
                else
                    notif_box_low.add(anim);
                (anim.get_parent() as Gtk.Box).reorder_child(anim, 0);

                anim.show_all();
            });
            notif.notify_property("urgency"); // Initially place the widget

            notifications.replace(notif.id, anim);
            switcher.visible_child_name = "main";
            anim.reveal_child = true;
            notif.start_timeout();
        });
        server.notification_closed.connect((id, reason) => {
            var n_notifs = server.get_n_notifications();
            if (n_notifs > 99)
                notif_counter.label = "∞";
            else
                notif_counter.label = "%d".printf(n_notifs);

            var empty = notifications.size() < 2;
            if (empty) switcher.visible_child_name = "empty";

            var view = notifications.lookup(id);
            view.reveal_child = false;
            Timeout.add(empty ? 0 : view.transition_duration, () => {
                notifications.remove(id);
                view.destroy(); // TODO: Fix g_object_unref critical from here
                return Source.REMOVE;
            });
        });
        server.invoke_added(); // Start sending the notif_added signal
    }

    private Gtk.Widget create_notification(Notification notif) {
        var root = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
        root.get_style_context().add_class("notification");
        root.margin = 4;

        var header = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 0);
        root.pack_start(header, false);
        var hdr_icon = new Gtk.Image();
        hdr_icon.pixel_size = 16;
        hdr_icon.margin_start = 6;
        hdr_icon.no_show_all = true;
        notif.bind_property("app-icon", hdr_icon, "gicon");
        hdr_icon.bind_property("gicon", hdr_icon, "visible", BindingFlags.SYNC_CREATE,
            (binding, from, ref to) => {
                to = ((Icon?) from != null);
                return true;
            });
        header.pack_start(hdr_icon, false);
        var hdr_lbl = new Gtk.Label(null);
        notif.bind_property("app-name", hdr_lbl, "label");
        hdr_lbl.margin_start = 6;
        header.pack_start(hdr_lbl, false);

        var dismiss = new Gtk.Button.from_icon_name("window-close-symbolic");
        dismiss.get_style_context().add_class(Gtk.STYLE_CLASS_FLAT);
        dismiss.clicked.connect(() => notif.dismiss());
        header.pack_end(dismiss, false);

        var data = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 0);
        root.pack_start(data);

        var icon = new Gtk.Image();
        icon.pixel_size = 40;
        icon.margin_start = 8;
        icon.no_show_all = true;
        notif.bind_property("image", icon, "gicon");
        icon.bind_property("gicon", icon, "visible", BindingFlags.DEFAULT,
            (binding, from, ref to) => {
                to = ((Icon?) from != null);
                return true;
            });
        data.pack_start(icon, false);

        var details = new Gtk.Box(Gtk.Orientation.VERTICAL, 2);
        details.margin_start = details.margin_end = 8;
        details.margin_bottom = 6;
        data.pack_start(details, true);
        var title = new Gtk.Label(null);
        notif.bind_property("title", title, "label", BindingFlags.DEFAULT,
            (binding, from, ref to) => {
                to = "<big>%s</big>".printf(from.get_string());
                return true;
            });
        title.use_markup = true;
        title.xalign = 0;
        title.wrap = true;
        title.wrap_mode = Pango.WrapMode.WORD;
        title.lines = 1;
        title.ellipsize = Pango.EllipsizeMode.END;
        details.pack_start(title);
        var body = new Gtk.Label(null);
        notif.bind_property("body", body, "label");
        body.bind_property("label", body, "visible", BindingFlags.DEFAULT,
            (binding, from, ref to) => {
                to = (from.get_string() != null);
                return true;
            });
        body.no_show_all = true;
        body.use_markup = true;
        body.xalign = 0;
        body.wrap = true;
        body.wrap_mode = Pango.WrapMode.WORD;
        body.lines = 2;
        body.ellipsize = Pango.EllipsizeMode.END;
        details.pack_start(body);

        var actions = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 0);
        actions.get_style_context().add_class(Gtk.STYLE_CLASS_LINKED);
        root.pack_start(actions, false);
        foreach (var action in notif.actions) {
            var acn_btn = new Gtk.Button.with_label(action.display);
            acn_btn.clicked.connect(() => action.invoke());
            actions.pack_start(acn_btn);
        }
        actions.show_all();

        return root;
    }
}

public Panel.Applet? create_applet() {
	return new Notif.Applet();
}
