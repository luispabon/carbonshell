[DBus (name = "org.freedesktop.Notifications")]
public class Notif.Server : Object {
	// Thread-safe singleton
	private static Once<Server> _inst;
	[DBus (visible = false)]
	public static Server obtain() {
	    return _inst.once(() => new Server());
	}

	private Server() {
	    Bus.own_name(
	        BusType.SESSION, "org.freedesktop.Notifications",
	        BusNameOwnerFlags.REPLACE | BusNameOwnerFlags.ALLOW_REPLACEMENT,
	        con => {
	            try {
	                con.register_object("/org/freedesktop/Notifications", this);
	            } catch (Error e) {
	                error("Notif: FdoServer: %s", e.message);
	            }
	        }, null, () => warning("Notif: FdoServer: Failed to obtain bus name")
	    );

		this.notifications = new GenericArray<Notification>();
		notification_closed.connect(handle_notification_closed);

		foreach (var n in Persist.obtain().load()) {
			n.from_persist = true;
			this.add_notification(n, true);
		}
	}

	// NOTIFICATION MANAGEMENT

	private GenericArray<Notification> notifications;

	[DBus (visible = false)]
	public int get_n_notifications() {
        return notifications.length;
	}

	[DBus (visible = false)]
	public Notification? get_notification(uint32 id) {
		Notification? result = null;
		notifications.foreach(notif => {
			if (notif.id == id)
			    result = notif;
		});
		return result;
	}

	[DBus (visible = false)]
	public void add_notification(Notification notif, bool nopersist = false) {
		var old = this.get_notification(notif.id);
		if (old != null) {
			old.replace(notif);
		} else notifications.add(notif);
		notif_added(notif);
		if (!nopersist) Persist.obtain().update();
	}

	[DBus (visible = false)]
	public void handle_notification_closed(uint32 id, uint32 reason) {
		var notification = this.get_notification(id);
		if (notification != null) {
			notifications.remove(notification);
			curr_id = notification.id; // Search for new ID from here
			notification.unref();
			Persist.obtain().update();
		}
	}

	private uint32 curr_id = 1;
	private uint32 generate_new_id() {
		if (curr_id == int.MAX) curr_id = 1; // Just in case it rolls over
		for (uint32 id = curr_id; id <= int.MAX; id++) {
			if (get_notification(id) == null) {
				curr_id = id;
				return id;
			}
		}
		error("NotifServer: Failed to generate ID for notification");
		return -2;
	}

	[DBus (visible = false)]
	public Notification[] get_all() {
		return notifications.data;
	}

	[DBus (visible = false)]
	public void invoke_added() {
		notifications.foreach(it => notif_added(it));
	}

	[DBus (visible = false)]
	public signal void notif_added(Notification notif);

	// PROTOCOL

	public string[] get_capabilities() {
		string[] abilities = {};
		abilities += "body";
		abilities += "body-markup";
		abilities += "icon-static";
		abilities += "actions";
		abilities += "persistence";
		//abilities += "sound"; TODO
		return abilities;
	}

	// NOTE: Capital name here so it doesn't conflict with GObject's notify.
	public uint32 Notify(string app_name,
						 uint32 replaces_id,
						 string app_icon,
					     string summary,
						 string body,
						 string[] actions,
						 HashTable<string, Variant> hints,
						 int32 expire_timeout) {
		// Create a notification object
		Notification n = new Notification();
		n.freeze_notify();
		n.app_name = app_name;
		n.id = replaces_id == 0 ? generate_new_id() : replaces_id;
		n.app_icon = (app_icon == "") ? null :
		    new ThemedIcon.from_names({app_icon + "-symbolic", app_icon});
		n.title = summary;
		n.body = body;
		n.from_persist = false; // We just recieved this notification

        n.timeout = expire_timeout;
		if (n.timeout > 0 && n.timeout < 2000) // If timeout is < 2s, make it 2s
		    n.timeout = 2000;

		// Populate the actions
		Action? action = null;
		Action[] tmp = {}; // Needed b/c can't do += to a public array
		foreach (var str in actions) {
		    if (action == null) {
		        action = new Action();
		        action.action = str;
		        action.notif_id = n.id;
		    } else {
		        if (str != "") {
		            action.display = str;
		            tmp += action;
		        }
		        action = null;
		    }
		}
		n.actions = tmp;

        // Deal with hints
		var transient = hints.lookup("transient"); // Don't persist
		n.transient = transient != null ? transient.get_boolean() : false;

		var resident = hints.lookup("resident"); // Don't close on action
		n.resident = resident != null ? resident.get_boolean() : false;

		var urgency = hints.lookup("urgency"); // Importance of the notification
		n.urgency = urgency != null ? urgency.get_byte() : 1;

		n.desktop_info = null;
		if ("desktop-entry" in hints) {
		    var desktop_id = hints.lookup("desktop-entry").get_string() + ".desktop";

		    if (desktop_id == "discord.desktop") // TODO: Don't do this
		        desktop_id = "com.discordapp.Discord.desktop";

		    var info = new DesktopAppInfo(desktop_id);
		    if (info != null) {
		        n.desktop_info = info;

		        // If the name is all lower case, chances are it isn't a display
		        // name. Override w/ app's name from desktop info
		        if (n.app_name == "" || n.app_name.ascii_down() == n.app_name)
		            n.app_name = info.get_name();

		        if (n.app_icon == null) {
		            n.app_icon = n.desktop_info.get_icon();
	                if (n.app_icon is ThemedIcon) { // Try to make it symbolic
	                    var themed = n.app_icon as ThemedIcon;
	                    themed.prepend_name(themed.names[0] + "-symbolic");
	                }
		        }
		    }
		}

		// If we still don't have an app name, give it one
        if (n.app_name == "")
            n.app_name = "Unknown";

		var image_data = hints.lookup("image-data");
		var image_path = hints.lookup("image-path");
		var leg_icon_data = hints.lookup("icon_data"); // Legacy hint name
	    if (image_data != null || (leg_icon_data != null && image_path == null)) {
            unowned VariantIter iter = image_data != null ? image_data.iterator() :
                leg_icon_data.iterator();

            int width = -1, height = -1, rowstride = -1,
                bits_per_sample = -1, channels = -1;
            bool has_alpha = false;
            uint8 byte = 0;
            uint8[] data = {};
            iter.next("i", &width);
            iter.next("i", &height);
            iter.next("i", &rowstride);
            iter.next("b", &has_alpha);
            iter.next("i", &bits_per_sample);
            iter.next("i", &channels);

            var array_iter = iter.next_value().iterator();
            while (array_iter.next("y", &byte))
                data += byte;

            n.image = new Gdk.Pixbuf.from_data(data, Gdk.Colorspace.RGB,
                has_alpha, bits_per_sample, width, height, rowstride);
	    } else if (image_path != null) {
            var path = image_path.get_string();
            if (path.has_prefix("file://")) {
                var fs_path = Uri.unescape_string(path).replace("file://", "");
                n.image = new Gdk.Pixbuf.from_file(fs_path);
            } else {
                n.image = new ThemedIcon(path);
            }
	    } else n.image = null;

		add_notification(n);
		return n.id;
	}

	public void close_notification(uint32 id) {
		notification_closed(id, CloseReason.SELF);
	}

	public void get_server_information(out string name, out string vendor,
	        out string version, out string spec_version) {
		name = "cSH-notif-server";
		vendor = "carbonOS";
		version = "0.0.2";
		spec_version = "1.2";
	}

	public signal void notification_closed(uint32 id, uint32 reason);

	public signal void action_invoked(uint32 id, string action_key);
}

/*
[DBus (name = "org.gtk.Notifications")]
public class Notif.GtkServer : Object {
    // Thread-safe singleton
	private static Once<GtkServer> _inst;
	[DBus (visible = false)]
	public static Server obtain(Server srv = null) {
	    return _inst.once(() => new GtkServer(srv));
	}

    private Server srv;
	private GtkServer(Server srv) {
	    this.srv = srv;
	    Bus.own_name(
	        BusType.SESSION, "org.gtk.Notifications",
	        BusNameOwnerFlags.REPLACE | BusNameOwnerFlags.ALLOW_REPLACEMENT,
	        con => {
	            try {
	                con.register_object("/org/gtk/Notifications", this);
	            } catch (Error e) {
	                error("Notif: GtkServer: %s", e.message);
	            }
	        }, null, () => warning("Notif: GtkServer: Failed to obtain bus name")
	    );

	    srv.action_invoked.connect((id, reason) => {

	    })
	}

	public void add_notification(string app_id, string id, HashTable<string, Variant> dict) {
        var num_id = 0; // TODO
        var title = dict.lookup("title").get_string();
        var body = dict.lookup("body").get_string();
        var is_urgent = false;
        if ("urgent" in dict)
            is_urgent = dict.lookup("urgent").get_boolean();

        var hints = new HashTable<string, Variant>(null, null);
        hints.replace("urgency", new Variant.byte(is_urgent ? 2 : 1));
        //hints.replace("desktop-entry", new Variant.string(app_id));

        var actions = new string[]{};

        num_id = srv.Notify("", num_id, "", title, body, actions, hints);
	}

	public void remove_notification(string app_id, string id) {
        var id = 0; // TODO
        srv.close_notification(id);
	}
}
*/

public enum CloseReason {
	EXPIRED = 1,
	DISMISSED = 2,
	SELF = 3,
	UNDEFINED = 4
}
