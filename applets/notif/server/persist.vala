class Notif.Persist : Object {
	// Thread-safe singleton
	private static Once<Persist> _inst;
	public static Persist obtain() {
	    return _inst.once(() => new Persist());
	}

	public void update() {
		try {
		    var cache = Environment.get_user_cache_dir();
		    DirUtils.create_with_parents(cache + "/cSH-notifications", 0775);
			File file = File.new_for_path(cache + "/cSH-notifications/store.json");
			DataOutputStream output = new DataOutputStream(file.replace(null, false, FileCreateFlags.NONE));
			foreach (Notification n in Server.obtain().get_all()) {
				if (!n.transient) output.put_string(n.to_string () + "\n");
			}
		} catch (Error e) {
			warning("Notif: Persist: Failed to save notifications");
		}
	}

	public Notification[] load() {
		Notification[] result = {};
		try {
			File file = File.new_for_path(
			    Path.build_filename(Environment.get_user_cache_dir(),
			    "cSH-notifications", "store.json"));
			DataInputStream input = new DataInputStream(file.read());
			string? line = null;
			while ((line = input.read_line()) != null) {
				var notification = Notification.from_data(line);
				result += notification;
			}
		} catch (Error e) {
			warning("Notif: Persist: Failed to load notifications.");
		}
		return result;
	}
}
