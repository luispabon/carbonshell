public class Notif.Notification : Object {
	public string app_name { set; get; }
	public uint32 id { set; get; }
	public Icon app_icon { set; get; }
	public string app_icon_str { set; get; }
	public Icon image { set; get; }
	public string image_str { set; get; }
	public string title { set; get; }
	public string body { set; get; }
	public int32 timeout { set; get; }
	public bool transient { set; get; }
	public bool resident { set; get; }
	public bool from_persist { set; get; }
	public uint8 urgency { set; get; }
	public DesktopAppInfo? desktop_info { set; get; }
	public Action[] actions { set; get; }

	private uint? timeout_id = null;

	// Actions

	public Action? get_action(string action) {
		foreach (Action act in this.actions) {
			if (act.action == action) return act;
		}
		return null;
	}

	public void invoke_action(string action) {
		this.get_action(action).invoke();
	}

	// Dismiss and timeout

	public void start_timeout() {
		if (timeout_id != null) Source.remove(timeout_id);
		var real_timeout = timeout;
		if (real_timeout == -1) // We have the freedom to pick a timeout
		    if (urgency == 2) // Never time out critical notifications
		        real_timeout = 0;
		    else if (urgency == 1) // Time out normal notifs in 1 hr
		        real_timeout = 60 * 60 * 1000;
		    else // Time out low priority notifs in 5 minutes
		        real_timeout = 5 * 60 * 1000;
		if (real_timeout == 0) return;
		timeout_id = Timeout.add(real_timeout, () => {
		    timeout_id = null;
		    this.dismiss(CloseReason.EXPIRED);
			return Source.REMOVE;
		});
	}

	public void dismiss(int reason = CloseReason.DISMISSED) {
	    if (timeout_id != null) Source.remove(timeout_id);
	    if (image_str.has_prefix("/"))
	        FileUtils.remove(image_str);
		Server.obtain().notification_closed(this.id, reason);
	}

	public string to_string() {
	    app_icon_str = app_icon.to_string();
	    if (image is Gdk.Pixbuf) {
            var path = Path.build_filename(Environment.get_user_cache_dir(),
                "cSH-notifications", "%s.png".printf(Uuid.string_random()));
            (image as Gdk.Pixbuf).save(path, "png");
            image_str = path;
	    } else image_str = image.to_string();
		var node = Json.gobject_serialize(this);
		return Json.to_string(node, false);
	}

	public static Notification from_data(string data) {
		var out = new Notification();
		out.freeze_notify();
		out.replace(Json.gobject_from_data(typeof(Notification), data) as Notification);
		return out;
	}

	public void replace(Notification notif) {
		this.app_name = notif.app_name;
		this.id = notif.id;
		if (notif.app_icon == null)
		    this.app_icon = Icon.new_for_string(notif.app_icon_str);
	    else
		    this.app_icon = notif.app_icon;
        if (notif.image == null)
            if (notif.image_str.has_prefix("/")) {
                this.image = new Gdk.Pixbuf.from_file(notif.image_str);
                FileUtils.remove(notif.image_str);
            } else
                this.image = Icon.new_for_string(notif.image_str);
        else
            this.image = notif.image;
		this.title = notif.title;
		this.body = notif.body;
		this.timeout = notif.timeout;
		this.transient = notif.transient;
		this.actions = notif.actions;
		notif.unref();
	}
}

public class Notif.Action : Object {
	public string display { set; get; }

	public string action { set; get; }
	public uint32 notif_id;

	public void invoke() {
		Server.obtain().action_invoked(notif_id, action);

		var notif = Server.obtain().get_notification(notif_id);
		if (!notif.resident) notif.dismiss();

	}
}
