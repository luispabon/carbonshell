private void update_dnd_toggle(Panel.CCToggle toggle) {
    toggle.icon_name = toggle.active ? "notifications-disabled-symbolic" :
        "preferences-system-notifications-symbolic";
}

public Panel.Applet? create_applet() {
    var settings = new Settings("sh.carbon.shell.panel");
    var toggle = new Panel.CCToggle();
    toggle.name = "Do Not\nDisturb";
    toggle.status = null;
    toggle.multiline = true;

    settings.bind("dnd", toggle, "active", SettingsBindFlags.DEFAULT);
    toggle.notify["active"].connect(() => update_dnd_toggle(toggle));
    update_dnd_toggle(toggle);

    Panel.Signals.obtain().register_cc_toggle(toggle);
    return null;
}
