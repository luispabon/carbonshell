public enum DeviceType {
	UNKNOWN = 0,
	BATTERY = 2,
	UPS = 3
}

public enum DeviceState {
	UNKNOWN = 0u,
	CHARGING = 1u,
	DISCHARGING = 2u,
	EMPTY = 3u,
	FULLY_CHARGED = 4u,
	PENDING_CHARGE = 5u,
	PENDING_DISCHARGE = 6u
}

public enum DeviceWarningLevel {
	UNKNOWN = 0,
	NONE = 1,
	DISCHARGING = 2,
	LOW = 3,
	CRITICAL = 4,
	ACTION = 5
}

[DBus (name = "org.freedesktop.UPower.Device")]
public interface Device : DBusProxy {
	public abstract void refresh() throws GLib.Error;
	public abstract uint Type { public owned get;}
	public abstract uint state { public owned get;}
	public abstract double percentage { public owned get;}
	public abstract double energy { public owned get;}
	public abstract double energy_full { public owned get;}
	public abstract double energy_rate { public owned get;}
	public abstract int64 time_to_empty { public owned get;}
	public abstract int64 time_to_full { public owned get;}
	public abstract bool is_present { public owned get;}
	public abstract string icon_name { public owned get;}
	public abstract uint warning_level { public owned get;}
	public abstract uint64 update_time { public owned get;public set;}
}

[DBus (name = "org.freedesktop.UPower.KbdBacklight")]
public interface KbdBacklight : DBusProxy {
    public abstract int32 get_brightness();
    public abstract int32 get_max_brightness();
    public abstract void set_brightness(int32 to);
    public signal void brightness_changed(int32 to);
    public signal void brightness_changed_with_source(int32 to, string source);
}

public Device? obtain_bat_device() {
	try {
		return Bus.get_proxy_sync<Device>(BusType.SYSTEM, "org.freedesktop.UPower", "/org/freedesktop/UPower/devices/DisplayDevice");
	} catch (Error e) {
		return null;
	}
}

public KbdBacklight? obtain_kbd_backlight() {
    try {
        var bus = Bus.get_proxy_sync<KbdBacklight?>(BusType.SYSTEM, "org.freedesktop.UPower", "/org/freedesktop/UPower/KbdBacklight");
        bus.call_sync("GetMaxBrightness", null, DBusCallFlags.NONE, -1);
		return bus;
	} catch (Error e) {
	    info("No keyboard backlight");
		return null;
	}
}
