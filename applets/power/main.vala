private Panel.CCInfo? create_bat_cc() {
    var info = new Panel.CCInfo();
    info.bind_property("icon-name", info, "panel-icon-name");
    info.icon_name = "battery-missing-symbolic";
    info.label = "Loading...";

    var settings = new Settings("org.gnome.desktop.interface");

    var dev = obtain_bat_device();
    if (dev == null) return null;
    dev.g_properties_changed.connect((a, b) => update_bat_info(info, dev,
        settings.get_boolean("show-battery-percentage")));
    update_bat_info(info, dev,
        settings.get_boolean("show-battery-percentage"));

    settings.changed["show-battery-percentage"].connect(() => {
        update_bat_info(info, dev,
            settings.get_boolean("show-battery-percentage"));
    });

    return info;
}

private void update_bat_info(Panel.CCInfo info, Device device, bool percent) {
    if (!device.is_present) { // Hide everything
        info.icon_name = null;
        info.label = null;
    } else if (device.Type == DeviceType.BATTERY) {
        var status = "";
        int64 seconds = -1;
        switch (device.state) {
            case DeviceState.FULLY_CHARGED:
                status = "Fully Charged";
                break;
            case DeviceState.CHARGING:
                seconds = device.time_to_full;
                break;
            case DeviceState.DISCHARGING:
                seconds = device.time_to_empty;
                break;
            case DeviceState.PENDING_CHARGE:
                status = "Not Charging";
                break;
            default:
                status = "Estimating...";
                break;
        }

        if (seconds != -1) {
            var hrs = seconds / 3600;
            var mins = (seconds / 60) - (60 * hrs);
            status = "%lld:%02lld Left".printf(hrs, mins);
        }

        var level = 10 * Math.floor(device.percentage / 10);
        var charging = (device.state == DeviceState.CHARGING) ? "-charging"
            : "";
        if (device.state == DeviceState.FULLY_CHARGED)
            info.icon_name = "battery-level-100-charged-symbolic";
        else
            info.icon_name = "battery-level-%0.f%s-symbolic"
                .printf(level, charging);
        if (!Gtk.IconTheme.get_default().has_icon(info.icon_name))
            info.icon_name = device.icon_name;

        var percentage = device.percentage;
        if (device.state == DeviceState.FULLY_CHARGED)
            percentage = 100;
        info.panel_tooltip = "%0.lf%%".printf(percentage);
        info.label = "%s (%0.lf%%)".printf(status, percentage);
    } else { // Unknown device
        info.icon_name = "battery-missing-symbolic"; // Battery w/ ?
        info.label = null;
    }

    if (percent) {
        info.panel_text = info.panel_tooltip;
        info.panel_tooltip = null;
    } else
        info.panel_text = null;
}

private Panel.CCCustom? create_brightness_slider(Panel.Signals sig) {
    var backlight = obtain_disp_backlight();
    if (backlight == null) return null;

    sig.parsed["brightness-up"].connect(() => {
        backlight.set_brightness(backlight.get_brightness() + 5);
    });
    sig.parsed["brightness-down"].connect(() => {
        backlight.set_brightness(backlight.get_brightness() - 5);
    });

    var root = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 8);
    var icon = new Gtk.Image.from_icon_name("display-brightness-symbolic",
        Gtk.IconSize.BUTTON);
    icon.tooltip_text = "Display Brightness";
    root.pack_start(icon, false);
    var scale = new Gtk.Scale.with_range(Gtk.Orientation.HORIZONTAL, 0, 100, 1);
    scale.digits = 0;
    scale.draw_value = false;
    scale.set_value(backlight.get_brightness());
    scale.change_value.connect((type, val) => {
        backlight.set_brightness((int) val);
        return false;
    });
    backlight.brightness_changed.connect(val => scale.set_value(val));
    root.pack_start(scale);

    backlight.bind_property("show-slider", root, "visible",
        BindingFlags.SYNC_CREATE);

    return new Panel.CCCustom(root);
}

private Panel.CCCustom? create_kbd_brightness_slider(Panel.Signals sig) {
    var backlight = obtain_kbd_backlight();
    if (backlight == null) return null;

    sig.parsed["kbd-brightness-up"].connect(() => {
        backlight.set_brightness(backlight.get_brightness() + 5);
    });
    sig.parsed["kbd-brightness-down"].connect(() => {
        backlight.set_brightness(backlight.get_brightness() - 5);
    });

    var root = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 8);
    var icon = new Gtk.Image.from_icon_name("keyboard-brightness-symbolic",
        Gtk.IconSize.BUTTON);
    icon.tooltip_text = "Keyboard Brightness";
    root.pack_start(icon, false);
    var scale = new Gtk.Scale.with_range(Gtk.Orientation.HORIZONTAL, 0, 100, 1);
    scale.digits = 0;
    scale.draw_value = false;
    scale.set_value(backlight.get_brightness());
    scale.change_value.connect((type, val) => {
        backlight.set_brightness((int32) val);
        return false;
    });
    backlight.brightness_changed.connect(val => scale.set_value(val));
    root.pack_start(scale);

    return new Panel.CCCustom(root);
}

public Panel.Applet? create_applet(bool in_lock) {
    var sig = Panel.Signals.obtain();

    var bat_info = create_bat_cc();
    if (bat_info != null)
        sig.register_cc_info(bat_info, true);

    var slider = create_kbd_brightness_slider(sig);
    if (slider != null)
        sig.register_cc_custom(slider);

    slider = create_brightness_slider(sig);
    if (slider != null)
        sig.register_cc_custom(slider);

	return null; // We don't want a regular applet
}
