public class DispBacklight : Object {

    construct {
        client = new GUdev.Client(new string[] {"backlight"});
        client.uevent.connect((action, device) => {
            repopulate();
            //TODO: This goes completely dark. set_brightness(percentage, device);
        });
        repopulate();
    }

    GUdev.Client client = null;
    List<GUdev.Device> devices = null;
    uint64 percentage = 0;

    public bool show_slider { set; get; }

    public uint64 get_brightness() {
        return percentage;
    }

    public void set_brightness(uint64 brightness, GUdev.Device? skip = null) {
        this.percentage = brightness;
        foreach (GUdev.Device dev in devices)
            if (skip == null || dev != skip)
                set_percent_on_dev(dev, brightness);
        brightness_changed(brightness);
    }

    public signal void brightness_changed(uint64 to);

    private void repopulate() {
        devices = client.query_by_subsystem("backlight");

        uint64 avg = 0;
        uint count = 0;
        foreach (GUdev.Device dev in devices)
            if (dev.get_sysfs_attr_as_int("max_brightness") != 0) {
                    avg += get_percent_on_dev(dev);
                    count++;
                }
        if (count == 0) {// NVIDIA cards cause floating point error
            percentage = 0;
            show_slider = false;
        } else {
            percentage = avg / count;
            show_slider = true;
        }
        //print(@"Repopulated. New perentage = $percentage\n");
    }

    private uint64 get_percent_on_dev(GUdev.Device dev) {
        uint64 max = dev.get_sysfs_attr_as_uint64("max_brightness");
        uint64 curr = dev.get_sysfs_attr_as_uint64("brightness");
        return (curr * 100) / max;
    }

    private void set_percent_on_dev(GUdev.Device dev, uint64 percent) {
        if (percent < 0) percent = 0;
        if (percent > 100) percent = 100;
        uint64 max = dev.get_sysfs_attr_as_uint64("max_brightness");
        uint64 new_value = (uint64) (max / 100.0) * percent;
        string filename = dev.get_sysfs_path() + "/brightness";
        //print(@"writing to %s: $percent%% = $new_value\n", filename);
        File f = File.new_for_path(filename);
        DataOutputStream o = new DataOutputStream(f.replace(null, false, FileCreateFlags.NONE));
        o.put_string("%llu".printf(new_value));
        o.close();
    }
}

public DispBacklight? obtain_disp_backlight() {
    try {
        return new DispBacklight();
    } catch (Error e) {
        return null;
    }
}
