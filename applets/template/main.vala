class FooApplet : Panel.Applet {
	construct {
		applet_name = "foo";

        var btn = Panel.create_img_button("foo-icon-symbolic", "Tooltip");
        var popover = Panel.create_popover(this, btn);
        popover.width_request = -1;
        popover.height_request = -1;

        var layout = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);

        // Rip-off Dazzle empty state
        layout.spacing = 8;
        layout.margin = 20;
        var image = new Gtk.Image.from_icon_name("org.gnome.Builder-symbolic",
            Gtk.IconSize.INVALID);
        image.pixel_size = 100;
        layout.pack_start(image);
        var title = new Gtk.Label("<big><b>In Development</b></big>");
        title.use_markup = true;
        layout.pack_start(title, false);
        var subtitle = new Gtk.Label("This part of carbonSHELL is unfinished");
        subtitle.get_style_context().add_class("dim-label");
        layout.pack_start(subtitle, false);

        popover.add(layout);
        layout.show_all();

        this.panel_item = btn;
	}
}

public Panel.Applet? create_applet() {
	return new FooApplet();
}
