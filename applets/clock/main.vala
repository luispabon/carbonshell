public Panel.Applet? create_applet(bool in_lock) {
    var settings = new Settings("org.gnome.desktop.interface");
    var info = new Panel.CCInfo();
    info.bind_property("label", info, "panel-tooltip");
    info.icon_name = null;

    Timeout.add(250, () => {
        var military = settings.get_enum("clock-format") == 0;
        var seconds = settings.get_boolean("clock-show-seconds");
        var fmt = new StringBuilder();
        fmt.append(military ? "%H" : "%l");
        fmt.append(":%M");
        if (seconds) fmt.append(":%S");
        if (!military) fmt.append(" %p");

        var now = new DateTime.now_local();
        info.panel_text = now.format(fmt.str);
        info.label = now.format("%a, %B %-e");
        return Source.CONTINUE;
    });

    Panel.Signals.obtain().register_cc_info(info);
	return null; // We don't want a regular applet
}
