private const int PINNED_APPS_LIMIT = 10;

class AppsListApplet : Panel.Applet {
	private Gtk.Box root;
	public Dazzle.ElasticBin elastic;
	private IconBox pinned_box;
	private IconBox unpinned_box;
	private Gtk.Separator unpinned_sep;
	//public Gtk.Revealer unpin_revealer;
	public Gtk.Box delete_box;
	private Settings settings;

	////////////////////////////////
	// Initialization and Widgets //
	////////////////////////////////

	construct {
		applet_name = "app-list";

		settings = new Settings ("sh.carbon.shell.panel");
		settings.changed["pinned-apps"].connect (invalidate_pinned);

		panel_item = get_panel_item (); // Inits the views
		invalidate_pinned (); // Load in the pinned apps
		invalidate_unpinned (); // Load in the unpinned apps

		Panel.Signals.obtain().parsed["launch-pinned"].connect((cmd, data) => {
		    int i = int.parse(data);
			if (pinned_box != null) {
				Gtk.Widget child = pinned_box.get_children ().nth_data (i);
				if (child is AppLauncher) {
					AppLauncher launcher = child as AppLauncher;
					Carbon.Utils.launch (launcher.app);
				}

			}
		});
	}
	
	public Gtk.Widget get_panel_item () {
		if (root == null) {
		    elastic = new Dazzle.ElasticBin();
			root = new Gtk.Box (Gtk.Orientation.HORIZONTAL, 0);
			elastic.add(root);

			Gtk.Button delete_button = new Gtk.Button.from_icon_name ("user-trash-symbolic", Gtk.IconSize.SMALL_TOOLBAR);
			//delete_button.get_style_context ().add_class ("titlebutton");
			delete_button.tooltip_text = "Unpin";
			Gtk.drag_dest_set (delete_button, Gtk.DestDefaults.ALL, {}, Gdk.DragAction.MOVE);
			Gtk.drag_dest_add_uri_targets (delete_button);
			delete_button.drag_data_received.connect ((ctx, x, y, data, info, time) => Gtk.drag_finish (ctx, true, true, time));
			delete_box = new Gtk.Box (Gtk.Orientation.HORIZONTAL, 8);
			delete_box.add (new Gtk.Separator (Gtk.Orientation.VERTICAL));
			delete_box.add (delete_button);
			/*unpin_revealer = new Gtk.Revealer ();
			unpin_revealer.transition_duration = 500;
			unpin_revealer.transition_type = Gtk.RevealerTransitionType.SLIDE_RIGHT;
			unpin_revealer.reveal_child = false;
			unpin_revealer.add (delete_box);
			root.pack_end (unpin_revealer);*/
			root.pack_end(delete_box);
			delete_box.show_all();
			delete_box.no_show_all = true;
			delete_box.hide();

			pinned_box = new IconBox (this, true);
			root.pack_start (pinned_box);

			unpinned_sep = new Gtk.Separator (Gtk.Orientation.VERTICAL);
			unpinned_sep.margin_left = 8;
			unpinned_sep.margin_right = 8;
			unpinned_sep.no_show_all = true;
			root.pack_start (unpinned_sep);
			
			unpinned_box = new IconBox (this);
			root.pack_start (unpinned_box);
		}

		return elastic;
	}


	/////////////////////////
	// Pinned box contents //
	/////////////////////////

	private void invalidate_pinned () {
		string[] pinned_apps = settings.get_strv("pinned-apps");
		
		// Limit the amount of pinned apps to PINNED_APPS_LIMIT
		if (pinned_apps.length > PINNED_APPS_LIMIT) {
			string[] limited_array = {};
			for (int i = 0; i < PINNED_APPS_LIMIT; i++)
				limited_array += pinned_apps[i];
			settings.set_strv("pinned-apps", limited_array);
			return;
		}
		
		pinned_box.empty(); // Empty out the pinned box

		// Fill up the pinned box
		foreach (string id in pinned_apps)
			pinned_box.add_launcher(id);

		// Render the new contents
		pinned_box.show_all();
	}

	public void update_pinned_apps_setting () {
		string[] new_value = {};
		foreach (Gtk.Widget child in pinned_box.get_children())
			if (child is AppLauncher) {
				AppLauncher launcher = child as AppLauncher;
				new_value += launcher.app.get_id().replace(".desktop", "");
				launcher.set_open(false);
			}
		settings.set_strv("pinned-apps", new_value);
		
		invalidate_unpinned(); // Recalculate which items don't need to be in the unpinned box any more
	}

	public AppLauncher? app_in_pinned(string app_id) {
		foreach (Gtk.Widget child in pinned_box.get_children())
			if (child is AppLauncher && (child as AppLauncher).app.get_id() == app_id) return child as AppLauncher;
		return null;
	}

	///////////////////////////
	// Unpinned Box Contents //
	///////////////////////////

	public void invalidate_unpinned() {
		// Fetch the list of open apps.
		// TODO: For real
		string[] open_apps = { "org.gnome.Epiphany.Devel", "org.gnome.Terminal" };

		// Empty out the unpinned box
		unpinned_box.empty();
		bool empty = true;

		// Populate the unpinned box		
		foreach (string app in open_apps) {
			AppLauncher? pinned = app_in_pinned(app + ".desktop");
			if (pinned == null) {
				AppLauncher launcher = unpinned_box.add_launcher(app);
				launcher.set_open(true);
				empty = false;
			} else pinned.set_open(true);
		}

		// Show/hide the pinned/unpinned divider
		if (empty)
			unpinned_sep.hide();
		else
			unpinned_sep.show();
		
		// Render the new contents
		unpinned_box.show_all();
	}
}

class IconBox : Gtk.Box {
	private Gtk.Widget drag_indicator;
	
	private bool update_settings = false;
	private AppsListApplet applet;

	public IconBox(AppsListApplet applet, bool is_pinned = false) {
		this.applet = applet;
		this.update_settings = is_pinned;

		//this.spacing = 0;
		init_dnd();
	}

	public AppLauncher? add_launcher (string id) {
		return do_insert_launcher(new AppLauncher.from_id(id));
	}

	private AppLauncher? do_insert_launcher(AppLauncher launcher) {
		launcher.applet = this.update_settings ? this.applet : null;
		launcher.show_all();
		this.add(launcher);
		return launcher;
	}

	public void empty () {
		foreach (Gtk.Widget child in this.get_children ())
			child.destroy ();
	}
	
	///////////////////
	// Drag and drop //
	///////////////////

	private void init_dnd() {
		Gtk.drag_dest_set(this, Gtk.DestDefaults.MOTION | Gtk.DestDefaults.DROP, {}, Gdk.DragAction.MOVE | Gdk.DragAction.COPY);
		Gtk.drag_dest_add_uri_targets(this);
	}

	private int calc_insert_index (int x) {
		int total_width = 0;
		int insert_index = 0;
		foreach (Gtk.Widget child in this.get_children ()) {
			int child_width = child.get_allocated_width ();
			if (child is Gtk.Separator) {
				total_width += child_width;
				continue;
			}
			total_width += (child_width / 2);
			if (x > total_width) insert_index++;
			total_width += (child_width / 2);
		}
		return insert_index;
	}

	public override bool drag_motion (Gdk.DragContext ctx, int x, int y, uint time) {
	 	// Create the indicator if necessary
		if (drag_indicator == null) {
			drag_indicator = new Gtk.Separator (Gtk.Orientation.VERTICAL);
			drag_indicator.show_all ();
			this.pack_start (drag_indicator);
			Gtk.drag_highlight (drag_indicator);
		}

		// Move the indicator
		this.reorder_child (drag_indicator, calc_insert_index (x));

		print("y=%d\n", y);
		return true;
	}

	public override void drag_leave (Gdk.DragContext ctx, uint time) {
		// Destroy the indicator
		if (drag_indicator != null) {
			drag_indicator.destroy ();
			drag_indicator = null;
		}
	}
	
	public override void drag_data_received (Gdk.DragContext ctx, int x, int y, Gtk.SelectionData data, uint info, uint time) {
		// Handle an error case
		if (data.get_length () < 0) {
			Gtk.drag_finish (ctx, false, false, time);
			return;
		}

		// Do the insertion
		int drop_index = calc_insert_index (x);
		foreach (string uri in data.get_uris ()) {
			if (!uri.has_suffix (".desktop")) continue;
			string path = File.new_for_uri (uri).get_path ();
			if (path == null) continue;

			AppLauncher? insert = do_insert_launcher (new AppLauncher.from_path (path));
			if (insert == null) continue;
			this.reorder_child (insert, drop_index);
			drop_index++;
		}

		// Clean up
		bool move = ctx.get_actions () == Gdk.DragAction.MOVE;
		if (update_settings) {
			if (move) this.remove (Gtk.drag_get_source_widget (ctx).get_parent());
			applet.update_pinned_apps_setting ();
		} else applet.invalidate_unpinned ();

		// Finalize
		applet.delete_box.hide();
		Gtk.drag_finish (ctx, true, move, time);
	}
}


class AppLauncher : Gtk.Overlay {
	public DesktopAppInfo app;
	public AppsListApplet? applet;

	private Icon app_icon;
	private bool data_destroyed;
	private bool open = false;

	private Gtk.Box indicator;

	public AppLauncher.from_id (string id) {
		this.from_info (new DesktopAppInfo(id + ".desktop"), @"with id: $id.");
	}

	public AppLauncher.from_path (string path) {
		this.from_info(new DesktopAppInfo.from_filename(path), @"from $path.");
	}

	public AppLauncher.from_info(DesktopAppInfo? info, string err_message = "from an unknown source.") {
		if (info == null) {
			stderr.printf("[cSH-panel] (apps-list) Attempted to load invalid app info %s\n", err_message);
			Idle.add (() => {
				this.destroy();
				if (applet != null) applet.update_pinned_apps_setting();
				return Source.REMOVE;
			});
			return;
		}

        Gtk.Button btn = new Gtk.Button();
	    this.app_icon = info.get_icon () != null ? info.get_icon () : new ThemedIcon ("application-x-executable");
		Gtk.Image icon_view = new Gtk.Image.from_gicon(this.app_icon, Gtk.IconSize.LARGE_TOOLBAR);
		icon_view.pixel_size = 30; // Force the icon size
		btn.tooltip_text = info.get_display_name();
		btn.get_style_context ().add_class(Gtk.STYLE_CLASS_FLAT);
		btn.clicked.connect(() => Carbon.Utils.launch(info));
        btn.add(icon_view);
        this.add(btn);

        indicator = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 0);
        indicator.get_style_context().add_class("cSH-open-indicator");
        indicator.height_request = 2;
        var place_top = new Settings("sh.carbon.shell.panel").get_boolean("place-top");
        indicator.valign = place_top ? Gtk.Align.START : Gtk.Align.END;
        indicator.halign = Gtk.Align.CENTER;
        this.add_overlay(indicator);

        // Menu
        Gtk.Menu menu = new Gtk.Menu();
        string[] actions = info.list_actions();
        foreach (string action in actions) {
            string action_disp = info.get_action_name(action);
            Gtk.MenuItem menu_action_item = new Gtk.MenuItem.with_label(action_disp);
            menu_action_item.activate.connect(() => {
                Carbon.Utils.launch(info, action);
            });
            menu.append(menu_action_item);
        }
        if (actions.length > 0) menu.append(new Gtk.SeparatorMenuItem());
        Gtk.MenuItem menu_todo = new Gtk.MenuItem.with_label("TODO: Window Management");
        menu.append(menu_todo);
        menu.attach_to_widget(btn, null);
        menu.show_all();
        btn.button_press_event.connect(evt => {
            if (evt.button == Gdk.BUTTON_SECONDARY) menu.popup_at_pointer(evt);
            return Gdk.EVENT_PROPAGATE;
        });

		this.app = info;
		init_dnd (btn);
	}

	////////////////////
	// Open indicator //
	////////////////////

	public void set_open (bool open) {
		this.open = open;
		if (open)
			indicator.get_style_context().add_class("active");
		else
			indicator.get_style_context().remove_class("active");
	}

	///////////////////
	// Drag And Drop //
	///////////////////

	private void init_dnd (Gtk.Button btn) {
		Gtk.drag_source_set(btn, Gdk.ModifierType.BUTTON1_MASK, {}, Gdk.DragAction.MOVE);
		Gtk.drag_source_add_uri_targets (btn);
		Gtk.drag_source_set_icon_gicon (btn, this.app_icon);

		btn.drag_begin.connect(ctx => {
		    btn.opacity = 0.5;
		    if (applet != null) applet.delete_box.show();
		    //applet.unpin_revealer.reveal_child = true;
		});
		btn.drag_end.connect(ctx => {
		    btn.opacity = 1;
		    if (applet != null) applet.delete_box.hide();
		    //applet.unpin_revealer.reveal_child = false;
		});
		btn.drag_data_get.connect((ctx, data, info, time) => {
		    string path = this.app.get_filename ();
		    data.set_uris ({ @"file://$path" });
		});
		btn.drag_data_delete.connect(ctx => {
		    if (data_destroyed) return;
		    destroy();
		    if (applet != null) applet.update_pinned_apps_setting ();
		    data_destroyed = true;
		});
	}
}

public Panel.Applet? create_applet (bool in_lock) {
    if (in_lock)
        return null;
    else
	    return new AppsListApplet();
}
