const string MAX_STG = "allow-volume-above-100-percent";
private class VolumeSlider : Panel.CCCustom {
    private Gtk.Image icon;
    private Gtk.Scale scale;

    private Settings settings;
    private Gvc.MixerControl mixer;
    private Gvc.MixerStream sink;

    private bool amplified;
    private double norm_max;
    private double max;
    private double incr;

    private bool suppress_upd = false;

    public VolumeSlider(Panel.Signals sig) {
        Object(); // Necessary or else complains about superclass constructor

        var root = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 8);
        var evt_box = new Gtk.EventBox();
        icon = new Gtk.Image();
        icon.tooltip_text = "System Volume";
        evt_box.add(icon);
        evt_box.button_release_event.connect(evt => {
            if (evt.button == Gdk.BUTTON_PRIMARY) sig.send("volume-mute");
            return Gdk.EVENT_STOP;
        });
        root.pack_start(evt_box, false);
        scale = new Gtk.Scale(Gtk.Orientation.HORIZONTAL, null);
        scale.draw_value = false;
        scale.value_changed.connect(() => {
            if (!suppress_upd) {
                sink.volume = (ulong) scale.get_value();
                sink.change_is_muted(false);
                Gvc.push_volume(sink);
            }
        });
        root.pack_start(scale);
        widget = root;

        settings = new Settings("org.gnome.desktop.sound");
        settings.@ref();
        settings.changed[MAX_STG].connect(() => setup_range());
        mixer = new Gvc.MixerControl("carbonSHELL Control Center");
        mixer.open();
        setup_range(true);
        mixer.default_sink_changed.connect(id => {
             setup_sink(mixer.lookup_stream_id(id));
        });

        sig.parsed["volume-up"].connect(() => {
            sink.change_is_muted(false);
            if (sink.volume + incr > max)
                sink.volume = (ulong) max;
            else
                sink.volume += (ulong) incr;
            Gvc.push_volume(sink);
        });
        sig.parsed["volume-down"].connect(() => {
            sink.change_is_muted(false);
            if (sink.volume <= incr)
                sink.volume = 0;
            else
                sink.volume -= (ulong) incr;
            Gvc.push_volume(sink);
        });
        sig.parsed["volume-mute"].connect(() => {
            sink.change_is_muted(!sink.is_muted);
        });
    }

    private void setup_sink(Gvc.MixerStream sink) {
        this.sink = sink;
        sink.notify.connect(p => {
            if (p.name == "volume" || p.name == "is-muted") {
                update_scale();
                update_icon();
            }
        });
        update_scale();
        update_icon();
    }

    private void setup_range(bool no_update_scale = false) {
        amplified = settings.get_boolean(MAX_STG);
        norm_max = mixer.get_vol_max_norm();
        max = amplified ? mixer.get_vol_max_amplified() : norm_max;
        incr = max / 20;

        if (amplified)
            scale.add_mark(norm_max, Gtk.PositionType.BOTTOM, "");
        else
            scale.clear_marks();

        if (!no_update_scale)
            update_scale();
    }

    private void update_scale() {
        scale.set_increments(incr, incr);
        scale.set_range(0, max);
        suppress_upd = true;
        scale.set_value(sink.is_muted ? 0 : sink.volume);
        suppress_upd = false;
    }

    private void update_icon() {
        double percent = sink.volume / norm_max;
        if (sink.is_muted || percent <= 0.0)
            this.panel_tooltip = "Muted";
        else
            this.panel_tooltip = "%0.f%%".printf(percent * 100);

        var lvl = "overamplified";
        if (percent <= 1.0) lvl = "high";
        if (percent <= 0.66) lvl = "medium";
        if (percent <= 0.33) lvl = "low";
        if (sink.is_muted || percent <= 0.0) lvl = "muted";
        panel_icon_name = icon.icon_name = "audio-volume-%s-symbolic".printf(lvl);
    }
}

public Panel.Applet? create_applet(bool in_lock) {
    var sig = Panel.Signals.obtain();
    var slider = new VolumeSlider(sig);
    slider.@ref(); // Otherwise Gvc doesn't load for whatever reason
    sig.register_cc_custom(slider);
	return null; // We don't want a regular applet
}
