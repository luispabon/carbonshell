[DBus (name = "org.freedesktop.login1.Manager")]
interface LoginInterface : Object {
	public abstract UnixInputStream inhibit(string what, string who,
	    string why, string mode) throws Error;
}

private class InhibitorToggle : Panel.CCToggle {
    private UnixInputStream? fd = null;

    public InhibitorToggle(Panel.Signals sig) {
        Object(); // Init superclass
        Gtk.IconTheme.get_default().add_resource_path("/csh");

        this.name = "Stay\nAwake";
        this.status = null;
        this.multiline = true;
        this.icon_name = "caffine-off";
        this.panel_tooltip = "Staying Awake";

        this.notify["active"].connect(() => toggled());
        sig.parsed["toggle-idle"].connect(() => {
            this.active = !this.active;
        });
    }

    private void toggled() {
        if (fd == null) {
            LoginInterface mgr = Bus.get_proxy_sync(BusType.SYSTEM,
                "org.freedesktop.login1", "/org/freedesktop/login1");
            fd = mgr.inhibit("idle", "carbonSHELL", "Stay Awake", "block");
        } else {
            fd.close();
            fd = null;
        }
        this.icon_name = this.active ? "caffine-on" : "caffine-off";
        this.panel_icon_name = this.active ? "caffine-on" : null;
    }
}

public Panel.Applet? create_applet() {
    var sig = Panel.Signals.obtain();
    var toggle = new InhibitorToggle(sig);
    toggle.@ref();
    sig.register_cc_toggle(toggle);
    return null;
}
