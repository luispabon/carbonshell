class OverviewApplet : Panel.Applet {
	construct {
		applet_name = "overview";

		var btn = new Gtk.Button.from_icon_name("focus-windows-symbolic");
		btn.get_style_context().add_class(Gtk.STYLE_CLASS_FLAT);
		btn.tooltip_text = "Overview";
		btn.clicked.connect(() => {
		    warning("TODO: Open overview");
		});
		panel_item = btn;
	}
}

public Panel.Applet? create_applet(bool in_lock) {
    if (in_lock)
        return null;
    else
	    return new OverviewApplet();
}
