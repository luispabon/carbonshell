class AppsGrid : Gtk.ScrolledWindow {
    Gtk.FlowBox apps_container;

    construct {
        this.hscrollbar_policy = Gtk.PolicyType.NEVER;

		apps_container = new Gtk.FlowBox();
		apps_container.selection_mode = Gtk.SelectionMode.NONE;
		apps_container.orientation = Gtk.Orientation.HORIZONTAL;
		apps_container.homogeneous = true;
		apps_container.set_sort_func((a, b) => { // Sort by app name
		    string a_name = (a as AppGridItem).app.get_name();
		    string b_name = (b as AppGridItem).app.get_name();
		    return a_name.collate(b_name);
		});
		this.add(apps_container);
    }
    
    public void invalidate() {
        foreach (Gtk.Widget child in apps_container.get_children()) // Remove the old apps
            child.destroy();
        foreach (AppInfo app in AppInfo.get_all()) { // Add in the new ones
		    DesktopAppInfo dapp = new DesktopAppInfo(app.get_id());
		    if (dapp.get_nodisplay() || dapp.get_string("X-SearchOnly") == "true")
		        continue; // Skip this one
		    apps_container.add(new AppGridItem(dapp));
		}
		apps_container.show_all();
    }

}

class AppGridItem : Gtk.FlowBoxChild {
    public DesktopAppInfo app;

    public AppGridItem(DesktopAppInfo app) {
        this.can_focus = false; // Only allow focus on the button

		Gtk.Box app_info = new Gtk.Box(Gtk.Orientation.VERTICAL, 8);
		Gtk.Image icon = new Gtk.Image.from_gicon(app.get_icon(), Gtk.IconSize.INVALID);
		icon.pixel_size = 52;
		app_info.add(icon);
		Gtk.Label name = new Gtk.Label(app.get_name());
		name.ellipsize = Pango.EllipsizeMode.END;
		name.lines = 1;
		name.margin_left = name.margin_right = 10;
		name.max_width_chars = 10;
		app_info.add(name);

		// Launcher button
		Gtk.Button launcher = new Gtk.Button();
		launcher.width_request = 150;
		launcher.get_style_context().add_class(Gtk.STYLE_CLASS_FLAT);
		launcher.add(app_info);
		launcher.clicked.connect(launch);

		// Details tooltip
		string? desc = app.get_description();
		if (desc != null && desc.length > 0) {
		    desc = desc.replace("&", "&amp;");
		    desc = @"\n$desc";
		} else desc = "";
		launcher.tooltip_markup = @"<b>$(app.get_display_name ())</b>$desc";
        
        this.app = app;
        this.add(launcher);

        // Menu
        Gtk.Menu menu = new Gtk.Menu();
        string[] actions = app.list_actions();
        foreach (string action in actions) {
            string action_disp = app.get_action_name(action);
            Gtk.MenuItem menu_action_item = new Gtk.MenuItem.with_label(action_disp);
            menu_action_item.activate.connect(() => {
                Carbon.Utils.launch(app, action);
            });
            menu.append(menu_action_item);
        }
        if (actions.length > 0) menu.append(new Gtk.SeparatorMenuItem());
        Gtk.MenuItem menu_pin = new Gtk.MenuItem.with_label("TODO: Pin");
        menu_pin.activate.connect(() => {

        });
        menu.append(menu_pin);
        Gtk.MenuItem menu_edit = new Gtk.MenuItem.with_label("Edit");
        menu_edit.activate.connect(() => {
            Process.spawn_command_line_async("cSH-edit-app-dialog " + app.get_id());
        });
        menu.append(menu_edit);
        Gtk.MenuItem menu_app_info = new Gtk.MenuItem.with_label("App Details");
        menu_app_info.activate.connect(() => {
            Bus.@get.begin(BusType.SESSION, null, (obj, res) => {
                var connection = Bus.@get.end(res);
                var remote_actions = DBusActionGroup.@get(connection, "org.gnome.Software", "/org/gnome/Software");
                var id = app.get_id().slice(0, -8);
                var args = new Variant("(ss)", "////%s/".printf(id), "");
                remote_actions.activate_action("details", args);
            });
        });
        menu.append(menu_app_info);
        menu.attach_to_widget(launcher, null);
        menu.show_all();
        launcher.button_press_event.connect(evt => {
            if (evt.button == Gdk.BUTTON_SECONDARY) menu.popup_at_pointer(evt);
            return Gdk.EVENT_PROPAGATE;
        });

		// Drag and drop
		Gtk.drag_source_set(launcher, Gdk.ModifierType.BUTTON1_MASK, null, Gdk.DragAction.COPY);
		Gtk.drag_source_add_uri_targets(launcher);
		launcher.drag_begin.connect(ctx => Gtk.drag_set_icon_gicon(ctx, app.get_icon(), 0, 0));
		launcher.drag_data_get.connect((ctx, data, info, time) => data.set_uris({ @"file://$(app.get_filename ())" }));
    }

    public void launch() {
        Carbon.Utils.launch(app);
    }
}
