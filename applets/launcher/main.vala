class LauncherApplet : Panel.Applet {
	construct {
		applet_name = "launcher";

		var btn = Panel.create_img_button("view-app-grid-symbolic",
            "Applications", 24);
        var popover = Panel.create_popover(this, btn);
        popover.width_request = 700;
        popover.height_request = 500;

		var layout = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
        var switcher = new Gtk.Stack();
        switcher.transition_type = Gtk.StackTransitionType.SLIDE_UP_DOWN;

        var search = new SearchList();
        switcher.add_named(search, "search");

        var apps = new AppsGrid();
        AppInfoMonitor.@get().changed.connect(() => {
            print("[cSH-panel] Apps List Changed. Repopulating launcher\n");
            apps.invalidate();
        });
        popover.show.connect(() => apps.invalidate());
        switcher.add_named(apps, "apps");
        switcher.show_all(); // Necessary to start with 'apps' visible
        switcher.visible_child_name = "apps"; // Make 'apps' visible by default

        var search_box = new Gtk.SearchEntry();
        search_box.margin_start = 2;
        search_box.margin_end = 2;
        search_box.margin_top = 2;
        search_box.search_changed.connect(() => {
            if (search_box.text != "") {
                switcher.visible_child_name = "search";
                search.search(search_box.text);
            } else {
                switcher.visible_child_name = "apps";
                search.end_search();
            }
        });
        search_box.activate.connect(() => {
            search.launch_first_result();
            popover.popdown();
        });
        popover.closed.connect(() => {
            search_box.text = "";
        });

        layout.pack_start(search_box, false);
        layout.pack_start(switcher);
	    layout.show_all();
		popover.add(layout);

		panel_item = btn;
	}
}

public Panel.Applet? create_applet(bool in_lock) {
	return new LauncherApplet();
}
