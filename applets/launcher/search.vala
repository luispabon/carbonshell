// TODO: Cancellable support
// TODO: Clean up
class SearchList : Gtk.ScrolledWindow {
    Gtk.FlowBox app_results;
    Gtk.Separator app_sep;
    Gtk.FlowBox remote_results_view;
    ListStore results;

    List<SearchProvider> providers;
	Settings search_settings = new Settings("org.gnome.desktop.search-providers");

    construct {
        this.hscrollbar_policy = Gtk.PolicyType.EXTERNAL;
        this.get_style_context().add_class(Gtk.STYLE_CLASS_BACKGROUND);
        
        Gtk.Box layout = new Gtk.Box(Gtk.Orientation.VERTICAL, 8);

        app_results = new Gtk.FlowBox();
		app_results.selection_mode = Gtk.SelectionMode.NONE;
		app_results.orientation = Gtk.Orientation.HORIZONTAL;
		app_results.homogeneous = true;
		Dazzle.ElasticBin app_elastic = new Dazzle.ElasticBin();
		app_elastic.add(app_results);
		layout.pack_start(app_elastic, false);

		app_sep = new Gtk.Separator(Gtk.Orientation.HORIZONTAL);
		app_sep.no_show_all = true;
		layout.pack_start(app_sep, false);

        string[] sort_order = search_settings.get_strv("sort-order");

        remote_results_view = new Gtk.FlowBox();
        remote_results_view.max_children_per_line = 1;
        remote_results_view.row_spacing = 8;
        remote_results_view.selection_mode = Gtk.SelectionMode.NONE;
        remote_results_view.set_sort_func((child1, child2) => {
            SearchResultView result1 = child1 as SearchResultView;
            SearchResultView result2 = child2 as SearchResultView;

            int index1 = -1;
            int index2 = -1;
            for (int i = 0; i < sort_order.length; i++) {
                if (sort_order[i] == result1.id) {
                    index1 = i;
                    if (index2 != -1) break;
                } else if (sort_order[i] == result2.id) {
                    index2 = i;
                    if (index1 != -1) break;
                }
            }

            if (index1 != -1 && index2 == -1) return -1;
            else if (index1 == -1 && index2 != -1) return 1;
            else if (index1 == -1 && index2 == -1) return result1.name.collate(result2.name);
            else return index1 - index2;
        });
        results = new ListStore(typeof(SearchResult));
        remote_results_view.bind_model(results, obj => new SearchResultView(obj as SearchResult));
        layout.pack_start(remote_results_view, false);

        this.add(layout);

        load_search_providers();
        end_search(); // Put into a state ready for the next search
    }
    
    public void search(string query) {
        int count = 0;
        clear_app_results();
        foreach (string** group in DesktopAppInfo.search(query)) {
            for (int i = 0; i < strv_length((string[]) group) && count < 8; i++) {
                string id = group[i];
                DesktopAppInfo app = new DesktopAppInfo(id);
                if (app.get_nodisplay())
		            continue; // Skip this one
                app_results.add(new AppGridItem(app));
                count++;
            }
            if (count >= 8) break;
        }
        app_results.show_all();
        if (count > 0)
            app_sep.show();
        else
            app_sep.hide();

        foreach (SearchProvider provider in providers)
            provider.search.begin(query, results, (obj, res) => provider.search.end(res));
    }

    public void launch_first_result() {
        Gtk.FlowBoxChild? first_app = app_results.get_child_at_index(0);
        SearchResultView? first_result = remote_results_view.get_child_at_index(0) as SearchResultView;
        if (first_app != null) {
            (first_app as AppGridItem).launch();
        } else if (first_result != null) {
            first_result.launch_first_subresult();
        }
        end_search();
    }

    public void end_search() {
        results.remove_all();
        clear_app_results();

        foreach (SearchProvider provider in providers)
            provider.end_search();
    
        app_sep.hide();
    }

    void clear_app_results() {
        foreach (Gtk.Widget child in app_results.get_children()) // Remove the old results
            child.destroy();
    }

    void load_search_providers() {
        providers = new List<SearchProvider>();
        List<string> ids = new List<string>();
        string[] disabled = search_settings.get_strv("disabled");
        foreach (string data_dir in Environment.get_system_data_dirs()) {
            string search_path = data_dir + "/gnome-shell/search-providers";
            Dir dir;
            try {
                dir = Dir.open(search_path);
            } catch (FileError e) {
                continue;
            }
            string? name = null;
            while ((name = dir.read_name()) != null) {
                KeyFile keyfile = new KeyFile();
                try {
                    keyfile.load_from_file(search_path + "/" + name, KeyFileFlags.NONE);
                    if (!keyfile.has_group("Shell Search Provider")) continue;
                    int version = keyfile.get_integer("Shell Search Provider", "Version");
                    if (version != 2) continue; // We only support version 2 at the moment
                    string desktop_id = keyfile.get_string("Shell Search Provider", "DesktopId");
                    if (ids.find_custom(desktop_id, strcmp) != null) continue; // Load each app once only
                    ids.append(desktop_id);
                    // NOTE: This is commented out for now b/c all flatpaks are, by defualt, DefaultDisabled=true
                    //bool default_disabled = keyfile.get_boolean("Shell Search Provider", "DefaultDisabled");
                    if (desktop_id in disabled) break;
                    string bus_name = keyfile.get_string("Shell Search Provider", "BusName");
                    string object_path = keyfile.get_string("Shell Search Provider", "ObjectPath");
                    DesktopAppInfo app = new DesktopAppInfo(desktop_id);
                    Bus.get_proxy.begin<DBusSearchInterface>(BusType.SESSION, bus_name, object_path, DBusProxyFlags.NONE, null, (obj, res) => {
                        DBusSearchInterface proxy = Bus.get_proxy.end(res);
                        providers.append(new SearchProvider(app, proxy));
                        print("[cSH-panel] Loaded search provider for %s\n", desktop_id);
                    });
                } catch (Error e) {
                    error("Failed to load search provider %s: %s\n", name, e.message);
                    continue;
                }
            }
        }
    }
}

class SearchProvider {
    string? previous_query = null;
    string[]? result_ids = null;
    DBusSearchInterface proxy;
    DesktopAppInfo app;
    SearchResult? result = null;
    HashTable<string, SearchSubResult> meta_cache;
    bool appended = false;

    public SearchProvider(DesktopAppInfo app, DBusSearchInterface proxy) {
        this.app = app;
        this.proxy = proxy;
    }

    public async void search(string query, ListStore output) {
        if (result_ids == null || previous_query == null || query.index_of(previous_query) != 0) {
            result_ids = yield proxy.get_initial_result_set(query.split(" "));
        } else {
            result_ids = yield proxy.get_subsearch_result_set(result_ids, query.split(" "));
        }
        previous_query = query;
        if (result_ids.length == 0) return; // We don't have any results

        if (result == null) {
            result = new SearchResult();
            result.app = this.app;
            result.subresults = new ListStore(typeof(SearchSubResult));
            result.launch_app.connect(() => proxy.launch_search.begin(previous_query.split(" "), (uint) get_real_time(), (obj, res) => {
                proxy.launch_search.end(res);
            }));
        }
        result.ids = result_ids;

        result.subresults.remove_all();
        for (int i = 0; i < result_ids.length; i++) {
            if (i >= 3 && result_ids[i].index_of("special:") != 0) continue;
            load_meta.begin(result_ids[i], result.subresults, (obj, res) => {
                bool ok = load_meta.end(res);
                if (ok && !appended) {
                    if (result != null) // Fixes a race condition that causes a crash
                        output.append(result);
                    appended = true;
                }
            });
        }
    }


    public async bool load_meta(string id, ListStore output) {
        uint timestamp = (uint) get_real_time();
        if (meta_cache == null) meta_cache = new HashTable<string, SearchSubResult>(str_hash, str_equal);
        if (meta_cache.contains(id)) {
            var obj = meta_cache.lookup(id);
            if (obj != null) // (I hope) fixes a race condition
                output.append(obj);
            return true;
        }

        SearchSubResult result = new SearchSubResult();
        HashTable<string, Variant>[] proxy_results = yield proxy.get_result_metas(new string[] { id });
        if (proxy_results.length == 0) return false;
        HashTable<string, Variant> metadata = proxy_results[0];
        if (metadata == null) return false;
        result.timestamp = timestamp;
        result.id = id;
        result.name = metadata.lookup("name").get_string();
        if (metadata.contains("description"))
            result.description = metadata.lookup("description").get_string();
        else
            result.description = null;
        if (metadata.contains("icon"))
            result.icon = Icon.deserialize(metadata.lookup("icon"));
        else if (metadata.contains("gicon"))
            result.icon = Icon.new_for_string(metadata.lookup("gicon").get_string());
        else
            result.icon = null;
        result.launch.connect(() => proxy.activate_result.begin(id, previous_query.split(" "), timestamp, (obj, res) => {
            proxy.activate_result.end(res);
        }));
        if (output != null) // Deal with the output getting reset
            output.append(result);

        meta_cache.insert(id, result);
        return true;
    }

    public void end_search() {
        result = null;
        appended = false;
        previous_query = null;
        meta_cache = null;
    }
}

class SearchResult : Object {
    public DesktopAppInfo app;
    public string[] ids;
    public ListStore subresults;

    public signal void launch_app();
}

class SearchSubResult : Object {
    public string id;
    public uint timestamp;

    public Icon? icon;
    public string name;
    public string? description;

    public signal void launch();
}

class SearchResultView : Gtk.FlowBoxChild {
    // For sorting
    public string id;
    public string name;

    private Gtk.FlowBox subresult_view;

    public SearchResultView(SearchResult result) {
        this.id = result.app.get_id();
        this.name = result.app.get_name();
        this.can_focus = false;

        Gtk.Box layout = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 6);
        this.add(layout);

        Gtk.Image app_icon = new Gtk.Image.from_gicon(result.app.get_icon(), Gtk.IconSize.INVALID);
        app_icon.pixel_size = 36;

        Gtk.Button btn = new Gtk.Button();
        btn.add(app_icon);
        btn.get_style_context().add_class(Gtk.STYLE_CLASS_FLAT);
        btn.clicked.connect(() => result.launch_app());

        Gtk.Box btn_size_limit = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
        btn_size_limit.pack_start(btn, false);
        layout.pack_start(btn_size_limit, false);

        layout.pack_start(new Gtk.Separator(Gtk.Orientation.VERTICAL), false);

        subresult_view = new Gtk.FlowBox();
        subresult_view.max_children_per_line = 1;
        subresult_view.selection_mode = Gtk.SelectionMode.NONE;
        subresult_view.bind_model(result.subresults, obj => new SearchSubResultView(obj as SearchSubResult));
        layout.pack_start(subresult_view);

        this.show_all();
    }

    public void launch_first_subresult() {
        SearchSubResultView? view = subresult_view.get_child_at_index(0) as SearchSubResultView;
        if (view != null) view.result.launch();
    }
}

class SearchSubResultView : Gtk.FlowBoxChild {
    public SearchSubResult result;

    public SearchSubResultView(SearchSubResult result) {
        this.result = result;
        this.can_focus = false;

        Gtk.Button btn = new Gtk.Button();
        btn.get_style_context().add_class(Gtk.STYLE_CLASS_FLAT);
        btn.clicked.connect(() => result.launch());
        this.add(btn);

        Gtk.Box layout = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 6);
        btn.add(layout);

        if (result.icon != null) {
            Gtk.Image icon = new Gtk.Image.from_gicon(result.icon, Gtk.IconSize.INVALID);
            icon.pixel_size = 24;
            layout.pack_start(icon, false);
        }

        Gtk.Box details = new Gtk.Box(Gtk.Orientation.VERTICAL, 2);
        details.margin_start = 4;
        layout.pack_start(details);

        Gtk.Label name = new Gtk.Label(result.name.split("\n")[0]);
        name.xalign = 0;
        name.use_markup = true;
        name.wrap = true;
        name.wrap_mode = Pango.WrapMode.WORD_CHAR;
        name.lines = 2;
        name.ellipsize = Pango.EllipsizeMode.END;
        name.max_width_chars = 60;
        details.pack_start(name);

        if (result.description != null) {
            Gtk.Label desc = new Gtk.Label(result.description.split("\n")[0]);
            desc.xalign = 0;
            desc.use_markup = true;
            desc.wrap = true;
            desc.wrap_mode = Pango.WrapMode.WORD_CHAR;
            desc.lines = 3;
            desc.ellipsize = Pango.EllipsizeMode.END;
            desc.max_width_chars = 60;
            details.pack_start(desc);
        }

        this.show_all();
    }
}

[DBus (name = "org.gnome.Shell.SearchProvider2")]
interface DBusSearchInterface : Object {
    public async abstract string[] get_initial_result_set(string[] query);
    public async abstract string[] get_subsearch_result_set(string[] old_results, string[] new_query);
    public async abstract HashTable<string, Variant>[] get_result_metas(string[] ids);
    public async abstract void activate_result(string id, string[] query, uint timestamp);
    public async abstract void launch_search(string[] query, uint timestamp);
}
