public Panel.Applet? create_applet() {
    var toggle = new Panel.CCToggle();
    toggle.name = "WiFi";
    toggle.bind_property("icon-name", toggle, "panel-icon-name");
    toggle.icon_name = "network-wireless-no-route-symbolic";
    toggle.bind_property("status", toggle, "panel-tooltip");
    toggle.status = "Unknown";
    toggle.active = true;

    Panel.Signals.obtain().register_cc_toggle(toggle);
    return null;
}
