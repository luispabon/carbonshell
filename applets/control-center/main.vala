class ControlCenterApplet : Panel.Applet {
	construct {
		applet_name = "control-center";
		var sig = Panel.Signals.obtain();

        var btn = Panel.create_button(null);
        var btn_layout = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 12);
        btn.add(btn_layout);
        this.panel_item = btn;

        var popover = Panel.create_popover(this, btn);
        popover.width_request = 350;
        var layout = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
        layout.margin = 12;

        var info_box = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 8);
        info_box.margin_bottom = 8;
        layout.add(info_box);
        sig.register_cc_info.connect((info, pack_start) => {

            var widget = create_img_label(info, "icon-name", "label");
            if (pack_start)
                info_box.pack_start(widget, false);
            else
                info_box.pack_end(widget, false);

            btn_layout.pack_end(create_panel_item(info));
        });

        var custom_box = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
        custom_box.margin_bottom = 10;
        layout.add(custom_box);
        sig.register_cc_custom.connect(custom => {
            custom.widget.show_all();
            custom_box.pack_end(custom.widget);

            btn_layout.pack_end(create_panel_item(custom));
        });

        var toggle_grid = new Gtk.Grid();
        toggle_grid.row_spacing = toggle_grid.column_spacing = 16;
        toggle_grid.row_homogeneous = toggle_grid.column_homogeneous = true;
        layout.add(toggle_grid);
        int count = 0;
        sig.register_cc_toggle.connect(toggle => {
            int row = (count / 3);
            int col = (count % 3);
            toggle_grid.attach(create_toggle_item(toggle), col, row);
            count++;

            btn_layout.pack_end(create_panel_item(toggle));
        });

        popover.add(layout);
        layout.show_all();
	}

	private Gtk.Widget create_panel_item(Panel.CCApplet obj) {
	    return create_img_label(obj, "panel-icon-name", "panel-text", "panel-tooltip");
	}

    private Gtk.Widget create_img_label(Panel.CCApplet obj, string img_prop,
                string label_prop, string? tooltip_prop = null) {
        var wrap = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 2);
        wrap.no_show_all = true;
        var img = new Gtk.Image();
        img.pixel_size = 16;
        img.no_show_all = true;
        wrap.add(img);
        var lbl = new Gtk.Label(null);
        lbl.use_markup = true;
        lbl.no_show_all = true;
        wrap.add(lbl);

        img.notify["visible"].connect(() => {
            wrap.visible = (img.visible || lbl.visible);
        });
        lbl.notify["visible"].connect(() => {
            wrap.visible = (img.visible || lbl.visible);
        });

        bind_with_visibility(obj, img_prop, img, "icon-name");
        bind_with_visibility(obj, label_prop, lbl, "label");

        if (tooltip_prop != null)
            obj.bind_property(tooltip_prop, wrap, "tooltip-text",
                BindingFlags.SYNC_CREATE | BindingFlags.BIDIRECTIONAL);

        wrap.show_all();
        return wrap;
    }

    private void bind_with_visibility(Panel.CCApplet obj, string src_prop,
            Gtk.Widget dest, string dest_prop) {
        obj.bind_property(src_prop, dest, dest_prop, BindingFlags.SYNC_CREATE);
        obj.bind_property(src_prop, dest, "visible", BindingFlags.SYNC_CREATE,
            (binding, from, ref to) => {
                to = (from.get_string() != null);
                return true;
            });
    }

    private Gtk.Widget create_toggle_item(Panel.CCToggle toggle) {
        var root = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);

        var btn = new Gtk.ToggleButton();
        btn.halign = Gtk.Align.CENTER;
        btn.get_style_context().add_class("image-button");
        btn.get_style_context().add_class("circular");
        btn.get_style_context().add_class("flat");
        btn.get_style_context().add_class("cctoggle");
        btn.margin_bottom = 6;
        root.pack_start(btn);
        var img = new Gtk.Image();
        img.margin = 12;
        img.pixel_size = 26;
        toggle.bind_property("icon-name", img, "icon-name",
            BindingFlags.SYNC_CREATE);
        toggle.bind_property("active", btn, "active",
            BindingFlags.SYNC_CREATE | BindingFlags.BIDIRECTIONAL);
        btn.add(img);

        var lbl = new Gtk.Label(null);
        lbl.halign = Gtk.Align.CENTER;
        lbl.justify = Gtk.Justification.CENTER;
        toggle.bind_property("name", lbl, "label", BindingFlags.SYNC_CREATE);
        root.pack_start(lbl, false);

        if (!toggle.multiline) {
            var status = new Gtk.Label(null);
            status.halign = Gtk.Align.CENTER;
            status.get_style_context().add_class("dim-label");
            toggle.bind_property("status", status, "label", BindingFlags.SYNC_CREATE);
            root.pack_start(status, false);
        }

        root.show_all();
        return root;
    }
}

public Panel.Applet? create_applet() {
	return new ControlCenterApplet();
}
