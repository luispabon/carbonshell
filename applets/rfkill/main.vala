private void update_rfkill_toggle(Panel.CCToggle toggle) {
    toggle.panel_icon_name = toggle.active ?
        "airplane-mode-symbolic" : null;
    toggle.panel_tooltip = toggle.active ?
        "Airplane Mode" : null;
    warning("TODO: Change airplane mode");
}

public Panel.Applet? create_applet() {
    //var settings = new Settings("sh.carbon.shell.panel");
    var toggle = new Panel.CCToggle();
    toggle.name = "Airplane\nMode";
    toggle.icon_name = "airplane-mode-symbolic";
    toggle.active = false;
    toggle.status = null;
    toggle.multiline = true;

    //settings.bind("dnd", toggle, "active", SettingsBindFlags.DEFAULT);
    toggle.notify["active"].connect(() => update_rfkill_toggle(toggle));
    update_rfkill_toggle(toggle);

    Panel.Signals.obtain().register_cc_toggle(toggle);
    return null;
}
