[CCode (cheader_filename = "gtk-layer-shell.h", lower_case_cprefix = "gtk_layer_")]
namespace LayerShell {
	[CCode (cname = "GtkLayerShellLayer", cprefix = "GTK_LAYER_SHELL_LAYER_", has_type_id = false)]
	public enum Layer {
		BACKGROUND, BOTTOM, TOP, OVERLAY, ENTRY_NUMBER
	}

	[CCode (cname = "GtkLayerShellEdge", cprefix = "GTK_LAYER_SHELL_EDGE_", has_type_id = false)]
	public enum Edge {
		LEFT, RIGHT, TOP, BOTTOM, ENTRY_NUMBER
	}

	void init_for_window(Gtk.Window window);
	void set_namespace(Gtk.Window window, string namespace);
	void set_layer(Gtk.Window window, Layer layer);
	void set_monitor(Gtk.Window window, Gdk.Monitor monitor);
	void set_anchor(Gtk.Window window, Edge edge, bool anchor_to_edge);
	void set_margin(Gtk.Window window, Edge edge, int size);
	void set_exclusive_zone(Gtk.Window window, int size);
	void auto_exclusive_zone_enable(Gtk.Window window);
	void set_keyboard_interactivity(Gtk.Window win, bool interactivity);
}
