class Panel.Window : Gtk.Window {
    Gtk.Box content;
    Gtk.Box left_applets;
    Gtk.Box center_applets;
    Gtk.Box right_applets;
    Settings settings;

    construct {
        this.get_style_context().remove_class("background");
        this.decorated = false;

        settings = new Settings("sh.carbon.shell.panel");
        settings.changed["autohide"].connect(() => invalidate_autohide());
        settings.bind("force-dark", Gtk.Settings.get_default(),
            "gtk_application_prefer_dark_theme", SettingsBindFlags.DEFAULT);

        // Setup layer shell
        LayerShell.init_for_window(this);
        LayerShell.set_layer(this, LayerShell.Layer.TOP);
        LayerShell.set_namespace(this, "$unfocus"); // Ask wf to sanely focus
        LayerShell.set_anchor(this, LayerShell.Edge.LEFT, true);
        LayerShell.set_anchor(this, LayerShell.Edge.RIGHT, true);
        var place_top = settings.get_boolean("place-top");
        LayerShell.set_anchor(this, LayerShell.Edge.TOP, place_top);
        LayerShell.set_anchor(this, LayerShell.Edge.BOTTOM, !place_top);

        // Apply custom CSS
        var css_provider = new Gtk.CssProvider();
        css_provider.load_from_resource("/csh/panel.css");
        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(), css_provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        );

        // Setup the shadow
        var root = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
        this.add(root);
        var shadow = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
        shadow.get_style_context().add_class(!place_top ? "shadow" : "shadow-inv");
        if (!place_top)
            root.pack_start(shadow, false, false);
        else
            root.pack_end(shadow, false, false);

        // Setup the panel's root
        content = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 0);
        content.get_style_context().add_class("cSH-panel");
        content.get_style_context().add_class("opaque"); // TODO: Read this in from app-list
        root.pack_start(content);

        // Setup the applet locations
        left_applets = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 2);
        content.pack_start(left_applets, false);
        left_applets.margin = 2;
        center_applets = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 2);
        content.set_center_widget(center_applets);
        right_applets = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 2);
        content.pack_end(right_applets, false);
        right_applets.margin = 2;

        // Respond to applet or bus requests
        var sig = Signals.obtain();
        sig.focus_window.connect(() =>
            LayerShell.set_keyboard_interactivity(this, true));
        sig.defocus_window.connect(() =>
            LayerShell.set_keyboard_interactivity(this, false));
        sig.invalidate_autohide.connect(() => invalidate_autohide());
        sig.peek.connect(duration => {
            if (peek_timer != null) Source.remove(peek_timer);
            peek_timer = Timeout.add(duration, () => {
                peek_timer = null;
                return invalidate_autohide();
            });
            invalidate_autohide();
        });

        // Load the plugins
        var ld = PluginLoader.obtain();
        ld.load();
        foreach (var applet in ld.get_applets()) {
            applet.window = this;
            var widget = applet.panel_item;
            switch (applet.placement) {
                case Gtk.Align.START:
                    left_applets.add(widget);
                    break;
                case Gtk.Align.CENTER:
                    center_applets.add(widget);
                    break;
                case Gtk.Align.END:
                    right_applets.add(widget);
                    break;
            }
            widget.show_all();
        }
    }

    // Auohide management

    bool dodging = false; // Currently dodging a window
    bool mouse_over = false; // The mouse is currently on the window
    bool hidden = true; // The window is currently hidden
    uint? peek_timer = null; // The ID of an active peek timeout
    uint mouse_off_timer = -1; // The ID of an active mouse-off timeout
    int curr_exclusive_zone = 0;

    public bool invalidate_autohide(bool first_run = false) {
        bool autohide = settings.get_boolean("autohide");

        bool applet_open = false;
        Applet[] applets = PluginLoader.obtain().get_applets();
        foreach (Applet applet in applets) {
            applet_open = applet.suppress_autohide;
            if (applet_open) break;
        }

        int height;
        height = content.get_allocated_height();
        //root.get_size(null, out height);

        int pixels_remaining = dodging ? 0 : 1;


        // TODO: libdazzle animation
        if ((autohide || dodging) && !applet_open && !mouse_over && peek_timer == null) {
            // Hide the window
            if (hidden && !first_run) return Source.REMOVE;
            int value = 0;
            Timeout.add(3, () => {
                value--;
                //Wayland.surface_set_margin(this, 0, value, 0, 0);
                LayerShell.set_margin(this, LayerShell.Edge.BOTTOM, value);
                if (curr_exclusive_zone != pixels_remaining) {
                    curr_exclusive_zone = value + height;
                    //Wayland.surface_set_exclusive_zone(this, curr_exclusive_zone);
                    LayerShell.set_exclusive_zone(this, curr_exclusive_zone);
                }
                return value > pixels_remaining - height;
            });
            hidden = true;
        } else {
            // Show the window
            if (!hidden && !first_run) return Source.REMOVE;
            int value = pixels_remaining - height;
            Timeout.add(3, () => {
                value++;
                //Wayland.surface_set_margin(this, 0, value, 0, 0);
                LayerShell.set_margin(this, LayerShell.Edge.BOTTOM, value);
                if (!autohide && !dodging) {
                    curr_exclusive_zone = value + height;
                    //Wayland.surface_set_exclusive_zone(this, curr_exclusive_zone);
                    LayerShell.set_exclusive_zone(this, curr_exclusive_zone);
                }
                return value < 0;
            });
            hidden = false;
        }

        return Source.REMOVE; // Support Timeout.add
    }

    public override bool enter_notify_event(Gdk.EventCrossing evt) {
        mouse_over = true;
        invalidate_autohide();
        return Gdk.EVENT_PROPAGATE;
    }

    public override bool leave_notify_event(Gdk.EventCrossing evt) {
        if (evt.detail != Gdk.NotifyType.INFERIOR) {
            mouse_over = false;
            int duration = settings.get_int("mouse-off-delay");
			if (mouse_off_timer != -1) Source.remove(mouse_off_timer);
			mouse_off_timer = Timeout.add (duration, () => {
			    mouse_off_timer = -1;
			    invalidate_autohide();
			    return Source.REMOVE;
			});
        }
        return Gdk.EVENT_PROPAGATE;
    }

    // Popover Hiding

    public override bool button_release_event(Gdk.EventButton evt) {
        void* data;
        evt.window.get_user_data(out data);
        if (data == null) return false;
        Gtk.Widget? widget = data as Gtk.Widget;
        if (widget == null) return false;

        if (widget.get_ancestor(typeof(Gtk.Popover)) == null)
            Signals.obtain().widget_clicked(widget);

        return false;
    }

    public override bool focus_out_event(Gdk.EventFocus evt) {
        Signals.obtain().hide_popovers("!!!");
        Signals.obtain().defocus_window();
        return false;
    }

    // Colors

    protected override void style_updated() {
        base.style_updated();
        Gtk.StyleContext ctx = content.get_style_context();
        Gtk.Settings settings = Gtk.Settings.get_default();

        bool dark = settings.gtk_application_prefer_dark_theme;
        if (!dark) dark = settings.gtk_theme_name.has_suffix("dark");
        if (!dark) {
            Gdk.RGBA rgba;
            var is_set = ctx.lookup_color("theme_bg_color", out rgba);
            dark = is_set && rgba.red + rgba.green + rgba.blue < 1.0;
        }

        var win_ctx = this.get_style_context();
        win_ctx.remove_class(dark ? "light" : "dark");
        win_ctx.add_class(dark ? "dark" : "light");
    }
}
