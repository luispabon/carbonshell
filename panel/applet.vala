public class Panel.Applet : Object {
    public Gtk.Window? window;
    public Gtk.Align placement;
    public string applet_name = "ERROR";
    public bool suppress_autohide = false;
    public bool suppress_defocus = false;
    public Gtk.Widget? panel_item = null;
}

namespace Panel {
    // Connects the necessary signals to a popover to make it behave properly
    public void setup_popover(Applet applet, Gtk.Popover popover,
            bool do_toggle = true) {
        Settings settings = new Settings("sh.carbon.shell.panel");

        popover.modal = false;
        popover.constrain_to = Gtk.PopoverConstraint.NONE;
        popover.position = settings.get_boolean("place-top") ?
            Gtk.PositionType.BOTTOM : Gtk.PositionType.TOP;
        popover.show.connect(() => {
            var sig = Signals.obtain();
            sig.hide_popovers(applet.applet_name);
            sig.focus_window();
            applet.window.set_focus(null);
            popover.grab_focus();

            applet.suppress_autohide = true;
            sig.invalidate_autohide();
        });
        popover.closed.connect(() => {
            var sig = Signals.obtain();
            if (!applet.suppress_defocus)
                sig.defocus_window();

            applet.suppress_autohide = false;
            sig.invalidate_autohide();
        });
        applet.window.key_press_event.connect(evt => {
            if (evt.keyval == Gdk.Key.Escape) popover.popdown();
            return Gdk.EVENT_PROPAGATE;
        });

        var sig = Signals.obtain();
        sig.hide_popovers.connect(not => {
            applet.suppress_defocus = (not != applet.applet_name);
            if (not != applet.applet_name) popover.popdown();
        });
        sig.widget_clicked.connect(widget => {
           if (widget != popover.relative_to) popover.popdown();
        });
        if (do_toggle)
            sig.toggle_popover[applet.applet_name].connect(() => {
                if (popover.visible)
                    popover.popdown();
                else
                    popover.popup();
            });
    }

    public Gtk.Popover create_popover(Applet applet, Gtk.MenuButton btn) {
        btn.popover = new Gtk.Popover(btn);
        btn.popover.bind_property("visible", btn, "active");
        setup_popover(applet, btn.popover);
        return btn.popover;
    }

    public Gtk.MenuButton create_button(string? tooltip = null) {
        var btn = new Gtk.MenuButton();
        btn.direction = Gtk.ArrowType.UP;
        btn.get_style_context().add_class(Gtk.STYLE_CLASS_FLAT);

        if (tooltip != null)
            btn.tooltip_text = tooltip;
        return btn;
    }

    public Gtk.MenuButton create_img_button(string icon_name,
        string? tooltip = null, int pixel_size = 18) {
        var btn = create_button(tooltip);

        btn.get_style_context().add_class("image-button");
        var img = new Gtk.Image.from_icon_name(icon_name, Gtk.IconSize.INVALID);
        img.pixel_size = pixel_size;
        btn.add(img);

        return btn;
    }
}

public class Panel.CCApplet : Object {
    public string? panel_icon_name {
        set; get;
        default = null;
    }

    public string? panel_text {
        set; get;
        default = null;
    }

    public string? panel_tooltip {
        set; get;
        default = null;
    }
}

public class Panel.CCInfo : Panel.CCApplet {
    public string? icon_name {
        set; get;
        default = "content-loading-symbolic";
    }

    public string? label {
        set; get;
        default = "Info";
    }
}

public class Panel.CCCustom : Panel.CCApplet {
    public CCCustom.empty() {}

    public CCCustom(Gtk.Widget w) {
        this.widget = w;
    }

    public Gtk.Widget widget;
}

public class Panel.CCToggle : Panel.CCApplet {
    public string? icon_name {
        set; get;
        default = "content-loading-symbolic";
    }

    //TODO: Emblem?

    public string name {
        get; set;
        default = "Toggle";
    }

    public bool multiline = false;

    public string? status {
        get; set;
        default = "State";
    }

    public bool active {
        get; set;
        default = false;
    }

    public Gtk.Widget? custom_more_settings = null;

    public string more_settings = "gnome-control-center.desktop";
}
