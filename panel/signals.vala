[DBus (name = "sh.carbon.Panel.Signals")]
public class Panel.Signals : Object {
    private static Once<Signals> _inst;
    public static Signals obtain() {
        return _inst.once(() => new Signals());
    }

    [DBus (visible = false)]
    public void register() {
        Bus.own_name(
            BusType.SESSION, "sh.carbon.Panel",
            BusNameOwnerFlags.DO_NOT_QUEUE,
            con => {
                try {
                    con.register_object("/sh/carbon/Panel/Signals", this);
                } catch (Error e) {
                    error("Signals: Failed to register on bus: %s", e.message);
                }
            }, null, () => error("Signals: Failed to obtain bus name")
        );
    }

    [DBus (visible = false)]
	public signal void focus_window(); // Take window focus

	[DBus (visible = false)]
	public signal void defocus_window(); // Give up window focus

	[DBus (visible = false)]
	public signal void invalidate_autohide();

	[DBus (visible = false)]
	public signal void peek(int duration);

    [DBus (visible = false)]
	public signal void hide_popovers(string not);

	[DBus (visible = false)]
	public signal void widget_clicked(Gtk.Widget target);

	[Signal (detailed = true)]
	[DBus (visible = false)]
	public signal void toggle_popover(string applet_name);

	[Signal (detailed = true)]
	[DBus (visible = false)]
	public signal void raw(string message);

	[Signal (detailed = true)]
	[DBus (visible = false)]
	public signal void parsed(string command, string? data);

    [DBus (visible = false)]
	public signal void register_cc_info(CCInfo info, bool left = false);

    [DBus (visible = false)]
	public signal void register_cc_custom(CCCustom info);

	[DBus (visible = false)]
	public signal void register_cc_toggle(CCToggle toggle);

	public void send(string message) {
	    string[] split = message.split(";", 2);
		string? cmd = split[0].strip();
		string? data = (split[1] != null ? split[1] : "").strip();

		switch (cmd) {
		    case "focus_window":
		        focus_window();
		        break;
		    case "defocus_window":
		        defocus_window();
		        break;
		    case "peek":
		        peek(int.parse(data));
		        break;
            case "hide_popovers":
                hide_popovers(data);
                break;
		    case "toggle_popover":
		        toggle_popover[data](data);
		        break;
		}

		raw[message](message);
        parsed[cmd](cmd, data);

        info("Signals: New Message: %s", message);
	}

}
