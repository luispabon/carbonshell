//TODO: Rewrite this
public class Carbon.Ticker : Object {
	construct {
		current_ticker = null;
		queue = new Queue<TickerData?> ();
		
		anim_wrapper = new Gtk.Revealer ();
		anim_wrapper.transition_type = Gtk.RevealerTransitionType.SLIDE_LEFT;
		anim_wrapper.reveal_child = false;
		anim_wrapper.transition_duration = 500;
		anim_wrapper.show_all ();
	}

	// Thread-safe singleton
	private static Once<Ticker> _inst;
	public static Ticker obtain () {
		return _inst.once (() => new Ticker ());
	}

	// Initial lock
	private bool locked = true;

	public void unlock () {
		init_ticker (anim_wrapper);
		locked = false;
		show_next_ticker ();
	}
	
	// Signals
	public signal void init_ticker (Gtk.Widget contents);
	[Signal (detailed = true)]
	public signal void ticker_show (string id);
	[Signal (detailed = true)]
	public signal void ticker_destroyed (string id);

	// To determine the order to show tickers
	public enum Priority {
		ADJUSTMENT = 4, // Used by volume/brightness adjustment sliders
		NOTIFICATION = 3, // Used to display notifications
		NORMAL = 2, // Generic middle-of-the-road priority
		PASSIVE = 1, // For passive information, such as "Connected to Wifi: Foo", "Current Song: Foo", or "Battery Finished Charging"
		BACKGROUND = 0 // For unimportant messages, such as "Welcome, $USER"
	}

	// A data structure for a Ticker
	private struct TickerData {
		public string id;
		public Gtk.Widget widget;
		public Priority priority;
		public uint duration;
		public uint timeout_id;
		public bool safe; // Is it safe to inturrupt this ticker?
		public bool timeout_started; // Was a timer ever started for this?
	}
	
	// Current State
	private TickerData? current_ticker;
	private Queue<TickerData?> queue;
	private Gtk.Revealer anim_wrapper;
	private bool dont_destroy = false; // When a ticker is interrupted, we don't want to destroy it fully when hiding it
	private bool interrupt_waiting = false;

	// Implementation functions

	private bool show_next_ticker () {
		if (locked || queue.is_empty ()) return Source.REMOVE;

		TickerData upcoming = queue.peek_head ();

		// Interrupt the current ticker if necessary
		if (current_ticker != null) {
			if (upcoming.priority > current_ticker.priority) {
				if (current_ticker.safe) {
					if (current_ticker.priority > Priority.PASSIVE) {
						dont_destroy = true;
						queue.push_nth (current_ticker, 1);
					}
					ticker_cancel_timeout ();
					ticker_start_hide_func ();
				} else interrupt_waiting = true;
			}
			return Source.REMOVE;
		}

		current_ticker = upcoming;
		queue.pop_head (); // Remove the new ticker from the queue

		// Connect mouse over events
		current_ticker.widget.enter_notify_event.connect (ticker_mouse_enter_func);
		current_ticker.widget.leave_notify_event.connect (ticker_mouse_leave_func);

		anim_wrapper.add (current_ticker.widget);
		current_ticker.widget.show_all ();
		anim_wrapper.reveal_child = true;
		current_ticker.safe = false;
		Timeout.add (anim_wrapper.transition_duration, () => ticker_init_timeout (true));
		return Source.REMOVE;
	}
	
	private bool ticker_init_timeout (bool change_safe) {
		if (change_safe) {
			current_ticker.safe = true; 
			if (interrupt_waiting) {
				interrupt_waiting = false;
				show_next_ticker (); // Trigger the interrupt
				return Source.REMOVE;
			}
		}
		ticker_show[current_ticker.id] (current_ticker.id);
		current_ticker.timeout_id = Timeout.add (current_ticker.duration, ticker_start_hide_func);
		current_ticker.timeout_started = true;
		return Source.REMOVE;
	}

	private void ticker_cancel_timeout () {
		if (current_ticker.timeout_id == -1) return;
		Source.remove (current_ticker.timeout_id);
		current_ticker.timeout_id = -1;
	}
		
	private bool ticker_start_hide_func () {
		anim_wrapper.reveal_child = false;
		current_ticker.safe = false;
		Timeout.add (anim_wrapper.transition_duration, ticker_destroy_func);
		return Source.REMOVE;
	}

	private bool ticker_destroy_func () {
		anim_wrapper.remove (current_ticker.widget);
		if (!dont_destroy) {
			current_ticker.widget.destroy ();
			ticker_destroyed[current_ticker.id] (current_ticker.id);
		} else dont_destroy = false;
		current_ticker = null;
		show_next_ticker ();
		return Source.REMOVE;
	}

	private int ticker_sort_func (TickerData? a, TickerData? b) {
		if (a == null || b == null || a.priority == b.priority) return 0;
		return a.priority > b.priority ? -1 : 1;
	}

	private bool ticker_mouse_enter_func (Gdk.EventCrossing evt) {
		print ("enter\n");
		ticker_cancel_timeout ();
		return Gdk.EVENT_PROPAGATE;
	}

	private bool ticker_mouse_leave_func (Gdk.EventCrossing evt) {
		if (current_ticker.timeout_started && evt.detail != Gdk.NotifyType.INFERIOR) {
			print ("leave\n");
			ticker_init_timeout (true);
		}
		return Gdk.EVENT_PROPAGATE;
	}

	private bool in_queue (string id) {
		return queue.search<string> (id, (t, d) => {
			return t.id == d ? 0 : -1;
		}) != null;
	}

	// Posting tickers

	public void post_with_id (string id, Gtk.Widget content, uint duration = 3000, Priority priority = Priority.ADJUSTMENT) {
		if (id != "generic" && (in_queue (id) || (current_ticker != null && current_ticker.id == id))) {
			refresh_id (id);
			return;
		}

		TickerData t = { id, content, priority, duration, -1, true, false };
		queue.insert_sorted (t, (a, b) => ticker_sort_func (a, b));
		Idle.add (show_next_ticker);
	}

	public void post (Gtk.Widget content, uint duration = 3000, Priority priority = Priority.NORMAL) {
		this.post_with_id ("generic", content, duration, priority);
	}

	public void post_message (string message, uint duration = 3000, Priority priority = Priority.PASSIVE, bool subtitle = false) {
		Gtk.Label content = new Gtk.Label (message);
		if (subtitle) content.get_style_context ().add_class (Gtk.STYLE_CLASS_DIM_LABEL);
		this.post (content, duration, priority);
	}

	// Restart a ticker's timeout clock, usually used for adjustments
	public void refresh_id (string id) {
		if (id != "generic" && current_ticker != null && current_ticker.id == id && current_ticker.safe) {
			ticker_cancel_timeout ();
			ticker_init_timeout (false);
		}
	}
}
