namespace Carbon.Utils {
	public void launch (AppInfo? app, string? action = null) {
		if (app == null) return;
		
		AppLaunchContext ctx = Gdk.Display.get_default ().get_app_launch_context ();
		if (app is DesktopAppInfo && action != null) 
			(app as DesktopAppInfo).launch_action (action, ctx);
		else try {
			app.launch (null, ctx);
		} catch {}
	}
}
