using GDesktop;
class Background.Window : Gtk.Window {
    Settings settings;
    public Gdk.Pixbuf pixbuf;
    TimedWallpaper timed = null;

    public Window() {
        decorated = false;
        title = "__cSH_background";
        draw.connect(cr => {
            cr.save();
            cr.scale(1.0 / this.scale_factor, 1.0 / this.scale_factor);
            Gdk.cairo_set_source_pixbuf(cr, pixbuf, 0, 0);
            cr.paint();
            cr.restore();
            return true;
        });
        size_allocate.connect(alloc => reset());

        LayerShell.init_for_window(this);
        LayerShell.set_exclusive_zone(this, -1); // Ignore panel exclusive zone
        LayerShell.set_layer(this, LayerShell.Layer.BACKGROUND);
        LayerShell.set_anchor(this, LayerShell.Edge.LEFT, true);
        LayerShell.set_anchor(this, LayerShell.Edge.RIGHT, true);
        LayerShell.set_anchor(this, LayerShell.Edge.TOP, true);
        LayerShell.set_anchor(this, LayerShell.Edge.BOTTOM, true);

        settings = new Settings("org.gnome.desktop.background");
        settings.changed.connect(opt => {
            if (opt == "picture-uri" || opt == "picture-options" ||
                opt == "primary-color" || opt == "picture-opacity")
                reset();
        });
    }

    public void reset(int width = -1, int height = -1) {
        if (width == -1 && height == -1) {
            this.get_size(out width, out height);
            width *= this.scale_factor;
            height *= this.scale_factor;
        }

        // Stop timed wallpaper if it exists
        if (timed != null) {
            timed.destroy();
            timed = null;
        }

        // Setup the base pixbuf (solid color)
        pixbuf = new Gdk.Pixbuf(Gdk.Colorspace.RGB, false, 8, width, height);
        var fill_color = Gdk.RGBA();
        if (fill_color.parse(settings.get_string("primary-color")))
            pixbuf.fill(fill_color.hash());
        else
            pixbuf.fill(0x000000FF); // Black

        // Check the background mode & picture-alpha
        var bg_mode = settings.get_enum("picture-options");
        var max_alpha = 255.0 * (settings.get_int("picture-opacity") / 100.0);
        if (bg_mode == BackgroundStyle.NONE || max_alpha == 0) {
            this.queue_draw();
            return;
        }

        // Get the background's path
        var uri = settings.get_string("picture-uri");
        var scheme = Uri.parse_scheme(uri);
        if (scheme != "file") {
            warning("%s:// URIs are not supported. Try file://", scheme);
            this.queue_draw();
            return;
        }
        var path = Uri.unescape_string(uri).substring(7);

        if (path.has_suffix(".xml")) { // GNOME timed wallpaper
            info("Setting timed wallpaper: %s", path);
            timed = new TimedWallpaper(this, path, bg_mode, (int) max_alpha);
        } else { // Regular Image
            info("Setting regular wallpaper: %s", path);
            try {
                var img = new Gdk.Pixbuf.from_file(path);
                pixbuf_blend(pixbuf, img, (int) max_alpha, bg_mode);
            } catch (Error e) {
                warning("Failed to load image: %s", e.message);
            }
        }

        this.queue_draw();
    }
}
