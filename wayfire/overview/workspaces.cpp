/* workspaces.cpp
 *
 * Copyright 2019 Adrian Vovk <adrianvovk@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "overview.hpp"

void Overview::reflow_workspaces(bool opening) { // TODO: This function
    // Setup the list of workspaces
    std::vector<OverviewWorkspace> new_workspaces;
    int n_workspaces = std::get<1>(output->workspace->get_workspace_grid_size());
    for (int i = 0; i < n_workspaces; i++) {
        /*double old_x = -1;
        double old_y = -1;
        double old_vi = -1;
        for (auto &ws : workspaces) // If this workspace was visible before, save its y pos
            if (ws.r_index == i) {
                old_x = ws.curr_x;
                old_y = ws.curr_y;
                old_vi = ws.v_index;
                break;
            }
        */

        OverviewWorkspace ws;
        ws.stream = new wf::workspace_stream_t;
        ws.stream->ws = {0, i};
        ws.r_index = i;
        //ws.v_index = old_vi == -1 ? i : old_vi;
        ws.v_index = i;
        //ws.curr_x = ws.x.start = ws.x.end = old_x;
        //ws.curr_y = ws.y.start = ws.y.end = old_y;
        if (get_wm_views({0, i}).size() == 0) {
            //ws.v_index = 1000;
            ws.empty = true;
        }
        new_workspaces.push_back(ws);
    }
    clear_workspaces(); // Delete the old workspaces
    workspaces = new_workspaces; // Use the new ones

    // Figure out a v index
    /*std::sort(workspaces.begin(), workspaces.end(), [](OverviewWorkspace a, OverviewWorkspace b) {
        return a.v_index < b.v_index;
    });
    for (unsigned int v_i = 0; v_i < workspaces.size(); v_i++)
        workspaces.at(v_i).v_index = v_i;
    */

    // Calculate x/y value based on v_index
    double ws_width = workspace_width->as_double();
    wf_geometry screen = output->get_relative_geometry();
    float scale = (ws_width - 20) / screen.width;

    //bool found_empty = false;
    for (auto &ws : workspaces) {
        //if (!found_empty || !ws.empty)
            ws.x.end = (10 + screen.width * scale);
        //else
        //    ws.x.end = -(ws.v_index * 6);
        //found_empty |= ws.empty;

        ws.y.end = 10 + (screen.height * scale + 10) * ws.v_index;
        //if (opening) {
            ws.curr_x = ws.x.start = ws.x.end;
            ws.curr_y = ws.y.start = ws.y.end;
        /*} else {
            ws.x.start = ws.curr_x;
            ws.y.start = ws.curr_y;
        }*/
    }
    reset_anim(ws_flow_duration);
}

void Overview::clear_workspaces() {
    for (auto &workspace : workspaces) {
        output->render->workspace_stream_stop(*workspace.stream);

        OpenGL::render_begin();
        workspace.stream->buffer.release();
        OpenGL::render_end();

        delete workspace.stream;
    }
    workspaces.clear();
}

void Overview::render_box(const wf_framebuffer& fb, wf_geometry screen, wlr_box geometry, wf_color color) {
    // Render the box
    OpenGL::render_begin(fb);
    float color_arr[4] = {color.r, color.g, color.b, color.a};
    float projection[9];
    wlr_matrix_projection(projection, screen.width, screen.height, WL_OUTPUT_TRANSFORM_NORMAL);
    float matrix[9];
    wlr_matrix_project_box(matrix, &geometry, WL_OUTPUT_TRANSFORM_NORMAL, 0, projection);
    wlr_render_quad_with_matrix(wf::get_core().renderer, color_arr, matrix);
    OpenGL::render_end();
}

void Overview::render_workspace_scrim(const wf_framebuffer& fb) {
    wf_geometry screen = output->get_relative_geometry();
    int width = workspace_width->as_double();
    int x = screen.width - (width * anim_duration.progress(workspace_slide_transition));
    render_box(fb, screen, {x, 0, width, screen.height}, {0.0, 0.0, 0.0, 0.7});
}

void Overview::render_workspace_preview(const wf_framebuffer& fb, OverviewWorkspace& ws) {
    double ws_width = workspace_width->as_double();
    wf_geometry screen = output->get_relative_geometry();

    float scale = (ws_width - 20) / screen.width;
    double preview_width = screen.width * scale;
    double preview_height = screen.height * scale;

    ws.curr_x = screen.width - ws_flow_duration.progress(ws.x) * anim_duration.progress(workspace_slide_transition);
    ws.curr_y = ws_flow_duration.progress(ws.y);

    int active_i = std::get<1>(output->workspace->get_current_workspace());
    if (active_i == ws.v_index) { // Draw Active indicator
        double thickness = 4 * ws_flow_duration.progress(workspace_slide_transition);
        render_box(fb, screen,
            {
                (int)(ws.curr_x - thickness), (int)(ws.curr_y - thickness),
                (int)(preview_width + 2 * thickness), (int)(preview_height + 2 * thickness)
            },
            active_color->as_cached_color());
    }

    OpenGL::render_begin(fb);
    auto scale_trans = glm::scale(glm::mat4(1.0), {scale, scale, 1});
    auto translate_trans = glm::translate(glm::mat4(1.0), {
        (2 * ws.curr_x - screen.width + preview_width) / preview_width,
        (screen.height - preview_height - 2 * ws.curr_y) / preview_height,
        0 });
    OpenGL::render_transformed_texture(ws.stream->buffer.tex,
        { -1, 1, 1, -1}, {}, scale_trans * translate_trans);
    GL_CALL(glUseProgram(0));
    OpenGL::render_end();
}
