namespace Lock.Utils {
    public bool hasBGRT() {
        return File.new_for_path("/sys/firmware/acpi/bgrt").query_exists();
    }

    public string get_username() {
        string user_text = Environment.get_real_name ();
		if (user_text == "Unknown") user_text = Environment.get_user_name ();
		return user_text;
    }

    public void set_busy(Gtk.Window win, bool busy) {
        if (busy) {
            win.get_application().mark_busy();
            win.get_window().set_cursor(new Gdk.Cursor.from_name(Gdk.Display.get_default(), "wait"));
            win.set_sensitive(false);
        } else {
            win.get_application().unmark_busy();
            win.get_window().set_cursor(null);
            win.set_sensitive(true);
        }
    }
}
