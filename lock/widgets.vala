using Panel;
namespace Lock.Widgets {
    class Account : Gtk.Box {
        public Account(string name, string icon, Gtk.Widget? embed) {
            Object(orientation: Gtk.Orientation.VERTICAL, spacing: 0);

            Gtk.Image image = new Gtk.Image.from_icon_name (icon, Gtk.IconSize.INVALID);
            image.pixel_size = 120;

            Gtk.Label label = new Gtk.Label (@"<span size=\"xx-large\">$name</span>");
            label.use_markup = true;

            this.pack_start (image);
            this.pack_start (label);
            if (embed != null)
                this.pack_end (embed);
        }
    }

    class Status : Gtk.Box {
        construct {
            this.orientation = Gtk.Orientation.HORIZONTAL;
            this.margin_top = 4;
            this.margin_bottom = 4;

            Gtk.Box applet_box = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 4);
            this.pack_end(applet_box, false);

            PluginLoader loader = PluginLoader.obtain();
            loader.applet_loaded.connect((applet, placement) => {
                //applet.window = this.get_window();
                Gtk.Widget widget = applet.get_panel_item();
                applet_box.add(widget);
                widget.show_all();
            });
            loader.load(true);
        }
    }
}
