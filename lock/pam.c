#include <security/pam_appl.h>
#include <glib.h>
#include <gtk/gtk.h>

typedef struct LockWindow LockWindow;
extern void lock_window_passwd_check_finished(LockWindow* win, gboolean auth_ok, const gchar* message);

pam_handle_t* handle = NULL; // The current PAM connection
gchar* pam_password = NULL; // The password that gets sent off to PAM
int auth_status = PAM_SUCCESS; // The current status of PAM

gchar* errstr(int err) {
  switch (err) {
  case PAM_SUCCESS:
    return "OK";
  case PAM_PERM_DENIED:
  case PAM_AUTH_ERR:
    return "Invalid Password";
  case PAM_MAXTRIES:
    return "Maximum Attempts Reached";
  default:
    return g_strdup_printf("Unknown Error (%d)", err);
  }
}

int pam_convo(int num_msg, const struct pam_message** msg, struct pam_response** resp, void* appdata) {
  struct pam_response* reply = g_new(struct pam_response, num_msg);
  if (reply == NULL) return PAM_SYSTEM_ERR; // Allocation Failed
  *resp = reply;
  for (int i = 0; i < num_msg; i++) {
    switch (msg[i]->msg_style) {
    case PAM_PROMPT_ECHO_ON:
    case PAM_PROMPT_ECHO_OFF:
      reply[i].resp = g_strdup(pam_password);
      if (reply[i].resp == NULL) return PAM_SYSTEM_ERR;
      break;
    default:
      break;
    }
  }
  return PAM_SUCCESS;
}

const struct pam_conv conv = {
  .conv = &pam_convo,
  .appdata_ptr = NULL
};

void lock_window_pam_start(LockWindow* win, gchar* username) {
  if (pam_start("cSH-lock", username, &conv, &handle) != PAM_SUCCESS) {
    g_error("[cSH-lock] pam_start failed\n");
    exit(1);
  }
}

void lock_window_pam_authenticate(LockWindow* win, gchar* password) {
  pam_password = password; // Export this for PAM
  auth_status = pam_authenticate(handle, 0); // Do the authentication
  lock_window_passwd_check_finished(win, auth_status == PAM_SUCCESS, errstr(auth_status));
}

void lock_window_pam_start_session(LockWindow* win) {
  // TODO
}

void lock_window_pam_end_session(LockWindow* win) {
  // TODO
}

void lock_window_pam_setenv(LockWindow* win, gchar* envstring) {
  // TODO
}

void lock_window_pam_end(LockWindow* win) {
  pam_end(handle, auth_status);
}
