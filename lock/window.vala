using Panel;
using Carbon; // TODO
class Lock.Window : Gtk.ApplicationWindow {
    Gtk.Entry user_passwd_entry;
    Gtk.Label user_passwd_err;
    bool inDM;

    public Window(Gtk.Application app/*, Background.Window bwin*/) {
        Object(application: app);
        inDM = Environment.get_variable("IN_cLM") != null;

        if (inDM) {
            var provider = new Gtk.CssProvider();
            provider.load_from_resource("csh/lock.css");
            Gtk.StyleContext.add_provider_for_screen(Gdk.Screen.get_default (), provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
            this.get_style_context().add_class("black");
        }

        var root = new Gtk.Box(Gtk.Orientation.VERTICAL, 4);
        this.add(root);

        var user_box = new Gtk.Box(Gtk.Orientation.VERTICAL, 8);
        user_box.halign = Gtk.Align.CENTER;
        var user_icn = new Gtk.Image.from_icon_name("avatar-default", Gtk.IconSize.INVALID);
        user_icn.pixel_size = 120;
        user_box.pack_start(user_icn, false, false);
        var user_name = new Gtk.Label("<span size=\"xx-large\">%s</span>".printf(Utils.get_username()));
        user_name.use_markup = true;
        user_box.pack_start(user_name, false, false);
        var user_passwd_box = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 0);
        user_passwd_box.get_style_context().add_class(Gtk.STYLE_CLASS_LINKED);
        user_passwd_entry = new Gtk.Entry();
        user_passwd_entry.placeholder_text = "Password";
        user_passwd_entry.input_purpose = Gtk.InputPurpose.PASSWORD;
        user_passwd_entry.caps_lock_warning = true;
        user_passwd_entry.visibility = false;
        user_passwd_entry.width_chars = 25;
        user_passwd_entry.grab_focus_without_selecting();
        user_passwd_entry.activate.connect(() => unlock());
        user_passwd_box.add(user_passwd_entry);
        var user_passwd_confirm = new Gtk.Button.from_icon_name("go-next-symbolic");
        user_passwd_confirm.clicked.connect(() => unlock());
        user_passwd_box.add(user_passwd_confirm);
        user_box.pack_start(user_passwd_box, false, false);
        user_passwd_err = new Gtk.Label("");
        user_box.pack_start(user_passwd_err, false, false);
        root.set_center_widget(user_box);

        var status = new Widgets.Status();
        if (inDM) {
            var quit = new Gtk.Button.from_icon_name("go-previous-symbolic");
            quit.clicked.connect(() => this.destroy());
            status.pack_start(quit, false, false);
        }
        root.pack_end(status, false, false);

        root.show_all();
        this.destroy.connect(() => {
            pam_end();
            app.release();
        });
        this.decorated = false;
    }

    public void unlock(string username = Environment.get_user_name()) {
        Utils.set_busy(this, true);
        new Thread<int>("pam auth", () => { // Run asnyc
            pam_start(username);
            pam_authenticate(user_passwd_entry.text);
            if (!inDM) pam_end(); // We don't interact further if we're not in the DM.
            return 0; // Does nothing
        });
    }

    extern void pam_start(string username);
    extern void pam_authenticate(string password);
    extern void pam_start_session();
    extern void pam_end_session();
    extern void pam_setenv(string expr);
    extern void pam_end();

    public void passwd_check_finished(bool auth_ok, string? error_message) {
        Utils.set_busy(this, false);
        if (auth_ok) this.destroy(); else {
            user_passwd_entry.grab_focus();
            user_passwd_err.label = error_message;
        }
    }

    public override bool button_release_event(Gdk.EventButton evt) {
        void* data;
        evt.window.get_user_data(out data);
        if (data == null) return false;
        Gtk.Widget? widget = data as Gtk.Widget;
        if (widget == null) return false;

        if (widget.get_ancestor(typeof(Gtk.Popover)) == null)
            Pipeline.obtain().widget_clicked(widget);

        return false;
    }

    public override bool focus_out_event(Gdk.EventFocus evt) {
        Pipeline.obtain().hide_popovers("!!!");
        return false;
    }
}
