namespace Lock {
    private static bool version = false;
    private const OptionEntry[] options = {
        { "version", 'v', 0, OptionArg.NONE, ref version, "Display version number", null },
        { null }
    };

    public int main (string[] args) {
	    Gtk.Application app = new Gtk.Application("sh.carbon.shell.lock", ApplicationFlags.HANDLES_COMMAND_LINE);
	    app.add_main_option_entries(options);
	    app.startup.connect(startup);
	    app.handle_local_options.connect(local_opts);
	    app.command_line.connect(cmdline => {
	        cmdline.print("[cSH-lock] Service is running.\n");
	        return 0;
	    });
        return app.run(args);
    }

    public void startup(Application app) {
        print("[cSH-lock] Starting\n");
        Gtk.Settings.get_default().gtk_application_prefer_dark_theme = true;
        

        Wayland.Callbacks cb = new Wayland.Callbacks();
        cb.new_output.connect((id, name, desc, x, y, width, height) => {
            /*Background.Window bwin = new Background.Window(app as Gtk.Application);
            Wayland.register_window(id, bwin, Wayland.Layer.OVERLAY);*/
        
            Lock.Window win = new Lock.Window(app as Gtk.Application/*, bwin*/);
            Wayland.register_window(id, win, Wayland.Layer.OVERLAY);
            Wayland.surface_configure(win, 0, 0);
            Wayland.surface_set_anchor(win, Wayland.Edge.TOP | Wayland.Edge.LEFT);
            Wayland.surface_set_keyboard_mode(win, Wayland.FocusMode.EXCLUSIVE);
        });
        cb.resize_window.connect((win, w, h) => {
           /*if (win is Background.Window)
            (win as Background.Window).invalidate_image(w, h);*/
        });
        Wayland.setup("cSH-lock", cb);

        app.hold(); // Don't exit
    }

    public int local_opts() {
        if (version) {
            print("carbonSHELL Lock\n");
            print("Version:%s\n", "0.0.0");
            print("GTK Version: %d.%d.%d\n", Gtk.MAJOR_VERSION, Gtk.MINOR_VERSION, Gtk.MICRO_VERSION);
            return 0;
        } else return -1;
    }
}
